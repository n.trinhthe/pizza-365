"use strict"
const gBASE_URL = "http://localhost:8080"
var gNameCol = ["id", "regionName", "regionCode", "countryName", "action"];
var gRegionList = [];
var gRegionId = "";
var gCountryId = "";
const gCOL_ID = 0;
const gCOL_REGION = 1;
const gCOL_REGION_CODE = 2;
const gCOL_COUNTRY = 3;
const gCOL_ACTION = 4;
var gTableRegion = $("#table-region").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_REGION] },
    { data: gNameCol[gCOL_REGION_CODE] },
    { data: gNameCol[gCOL_COUNTRY] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<button class="btn btn-primary btn-select"><i class="far fa-square fa-xl"></i> &nbsp Select</button>`,
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllRegion();
  getAllCountry();
})
$("#table-region tbody").on("click", ".btn-select", function () {
  onChangeColorButton(this);
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableRegion.row(vRowClick).data();
  console.log(vRowData);
  gRegionId = vRowData.id;
  loadDataToForm();
})
$("#btn-add-region").on("click", function () {
  onBtnAddRegion();
  $(document).find("input").val("");
})

$("#btn-edit-region").on("click", function () {
  onBtnEditRegion()
})
$("#btn-delete-region").on("click", function () {
  $("#delete-region-modal").modal("show");
})

$("#btn-confirm-delete-region").on("click", function () {
  onBtnConfirmDeleteRegion()
})
function loadDataToForm() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/regions/" + gRegionId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-region").val(res.regionName);
      $("#inp-region-code").val(res.regionCode);
      getCountryByCode(res.countryCode);
      console.log(gCountryId);
      $("#select-country").val(gCountryId).change();
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function getCountryByCode(paramCode) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/countries/code/" + paramCode,
    dataType: "json",
    type: "GET",
    async: false,
    success: function (res) {
      console.log(res);
      gCountryId = res.id;
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function getAllRegion() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/regions",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gRegionList = res;
      loadDataToTable(gRegionList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
//Hàm xử lý nút select
function onChangeColorButton(paramButton) {
  $("#table-region tbody").find("button").removeClass().addClass("btn btn-primary btn-select");
  $("#table-region tbody .btn-primary i").removeClass().addClass("text-light fas fa-square fa-xl");
  $(paramButton).removeClass().addClass("btn btn-success btn-select");
  $("#table-region tbody .btn-success i").removeClass().addClass("text-light fas fa-check-square fa-xl")
}
function loadDataToTable(pRegionList) {
  "use strict"
  gTableRegion.clear();
  gTableRegion.rows.add(pRegionList);
  gTableRegion.draw();
}
//Hàm xư lý nút xác nhận thêm Region
function onBtnAddRegion() {
  let vRegionObj = {
    regionName: "",
    regionCode: ""
  }
  let vCountryId = $("#select-country").val();
  console.log(vCountryId)
  getDataByForm(vRegionObj);
  var vCheck = validateData(vRegionObj);
  console.log(vCheck)
  if (vCountryId != -1) {
    if (vCheck == true) {
      $.ajax({
        url: gBASE_URL + "/countries/" + vCountryId + "/regions",
        type: "POST",
        data: JSON.stringify(vRegionObj),
        contentType: "application/json;charset=utf8",
        dataType: "json",
        success: function (res) {
          alert("Thêm Region thành công");
          getAllRegion();
          loadDataToTable(gRegionList);
          clearForm();
        },
        error: function (err) {
          alert("Thêm Region thất bại" + err.responseText);
        }
      })
    }
  }
  else {
    alert("Chưa chọn country")
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Region
function onBtnEditRegion() {
  let vRegionObj = {
    regionName: "",
    regionCode: ""
  }
  let vCountryId = $("#select-country").val();
  getDataByForm(vRegionObj);
  var vCheck = validateData(vRegionObj);
  if (vCountryId != -1) {
    if (vCheck == true) {
      $.ajax({
        url: gBASE_URL + "/countries/" + vCountryId + "/regions/" + gRegionId,
        type: "PUT",
        data: JSON.stringify(vRegionObj),
        contentType: "application/json;charset=utf8",
        dataType: "json",
        success: function (res) {
          console.log("Chỉnh sửa Region thành công");
          getAllRegion();
          loadDataToTable(gRegionList);
          clearForm();
        },
        error: function (err) {
          alert("Chỉnh sửa Region thất bại" + err.responseText);
        }
      })
    }
  }
  else {
    alert("Chưa chọn country")
  }

}
//Hàm xử lý nút xác nhận delete Region
function onBtnConfirmDeleteRegion() {
  $.ajax({
    url: gBASE_URL + "/regions/" + gRegionId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Region thành công");
      getAllRegion();
      loadDataToTable(gRegionList);
      clearForm();
      $("#delete-region-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Region thất bại" + err.responseText);
    }
  })
}

//Hàm truy xuất dữ liệu form
function getDataByForm(paramObj) {
  paramObj.regionName = $("#inp-region").val();
  paramObj.regionCode = $("#inp-region-code").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.regionName == "") {
    alert("Chưa nhập tên quốc gia!");
    vResult = false;
  }
  else if (paramObj.regionCode == "") {
    alert("Chưa nhập mã quốc gia!");
    vResult = false;
  }
  return vResult;
}
function getRegionById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/details/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-region-edit-modal").val(res.regionName);
      $("#inp-region-code-edit-modal").val(res.regionCode);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function getAllCountry() {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/countries",
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      loadDataToSelect(res);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}

function loadDataToSelect(paramRes) {
  $("#select-country").find("option").remove();
  $("#select-country").append($("<option>", {
    value: "-1",
    text: "Chọn Country"
  }))
  paramRes.forEach(element => {
    $("#select-country").append($("<option>", {
      value: element.id,
      text: element.countryName
    }));
  });
}

function clearForm() {
  $(document).find("input").val("");
  $('#select-country option').each(function () {
    if ($(this).val() != -1) {
      $(this).remove();
    }
  });
  getAllCountry();
}