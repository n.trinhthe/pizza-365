"use strict"
const gBASE_URL = "http://localhost:8080/menu"
var gNameCol = ["id", "kichCo", "duongKinh","salad","suon","soLuongNuoc","price","action"];
var gMenuList = [];
var gMenuId = "";
const gCOL_ID = 0;
const gCOL_KICH_CO = 1;
const gCOL_DUONG_KINH= 2;
const gCOL_SALAD = 3;
const gCOL_SUON = 4;
const gCOL_SO_LUONG_NUOC = 5;
const gCOL_GIA = 6;
const gCOL_ACTION = 7;
var gTableMenu = $("#table-menu").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_KICH_CO] },
    { data: gNameCol[gCOL_DUONG_KINH] },
    { data: gNameCol[gCOL_SALAD] },
    { data: gNameCol[gCOL_SUON] },
    { data: gNameCol[gCOL_SO_LUONG_NUOC] },
    { data: gNameCol[gCOL_GIA] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllMenu();
})
$("#btn-add-menu").on("click", function () {
  $("#add-menu-modal").modal("show");
  $("#add-menu-modal").find("input").val("")
})
$("#btn-confirm-add-menu").on("click", function () {
  onBtnConfirmAddMenu()
})
$("#table-menu tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableMenu.row(vRowClick).data();
  // console.log(vRowData);
  gMenuId = vRowData.id;
  $("#edit-menu-modal").modal("show");
  getMenuById(gMenuId);
})
$("#btn-confirm-edit-menu").on("click", function () {
  onBtnConfirmEditMenu()
})
$("#table-menu tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableMenu.row(vRowClick).data();
  console.log(vRowData);
  gMenuId = vRowData.id;
  $("#delete-menu-modal").modal("show");
})
$("#btn-confirm-delete-menu").on("click", function () {
  onBtnConfirmDeleteMenu()
})
function getAllMenu() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gMenuList = res;
      loadDataToTable(gMenuList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pMenuList) {
  "use strict"
  gTableMenu.clear();
  gTableMenu.rows.add(pMenuList);
  gTableMenu.draw();
}
//Hàm xư lý nút xác nhận thêm Menu
function onBtnConfirmAddMenu() {
  let vMenuObj = {
    kichCo: "",
    duongKinh: "",
    salad: "",
    suon: "",
    soLuongNuoc: "",
    price: ""
  }
  getDataByFormAddModal(vMenuObj);
  var vCheck = validateData(vMenuObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vMenuObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Thêm Menu thành công");
        getAllMenu();
        loadDataToTable(gMenuList);
        $("#add-menu-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Menu thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Menu
function onBtnConfirmEditMenu() {
  let vMenuObj = {
    kichCo: "",
    duongKinh: "",
    salad: "",
    suon: "",
    soLuongNuoc: "",
    price: ""
  }
  getDataByFormEditModal(vMenuObj);
  var vCheck = validateData(vMenuObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gMenuId,
      type: "PUT",
      data: JSON.stringify(vMenuObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa Menu thành công");
        getAllMenu();
        loadDataToTable(gMenuList);
        $("#edit-menu-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Menu thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Menu
function onBtnConfirmDeleteMenu() {
  $.ajax({
    url: gBASE_URL + "/" + gMenuId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Menu thành công");
      getAllMenu();
      loadDataToTable(gMenuList);
      $("#delete-menu-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Menu thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.kichCo = $("#inp-kich-co-edit-modal").val();
  paramObj.duongKinh = $("#inp-duong-kinh-edit-modal").val();
  paramObj.salad = $("#inp-salad-edit-modal").val();
  paramObj.suon = $("#inp-suon-edit-modal").val();
  paramObj.soLuongNuoc = $("#inp-so-luong-nuoc-edit-modal").val();
  paramObj.price = $("#inp-gia-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.kichCo = $("#inp-kich-co-add-modal").val();
  paramObj.duongKinh = $("#inp-duong-kinh-add-modal").val();
  paramObj.salad = $("#inp-salad-add-modal").val();
  paramObj.suon = $("#inp-suon-add-modal").val();
  paramObj.soLuongNuoc = $("#inp-so-luong-nuoc-add-modal").val();
  paramObj.price = $("#inp-gia-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.maMenu == "") {
    alert("Chưa nhập mã giảm giá!");
    vResult = false;
  }
  else if (paramObj.phanTramGiamGia == "") {
    alert("Chưa nhập phần trăm giảm giá!");
    vResult = false;
  }
  return vResult;
}
function getMenuById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-kich-co-edit-modal").val(res.kichCo);
      $("#inp-duong-kinh-edit-modal").val(res.duongKinh);
      $("#inp-salad-edit-modal").val(res.salad);
      $("#inp-suon-edit-modal").val(res.suon);
      $("#inp-so-luong-nuoc-edit-modal").val(res.soLuongNuoc);
      $("#inp-gia-edit-modal").val(res.price);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}