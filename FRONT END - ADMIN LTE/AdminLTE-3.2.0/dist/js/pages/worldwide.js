"use strict"
$(document).ready(function () {
    callAjaxCountryList();
})
$("#select-country").on("change", function () {
    console.log("Country được thay đổi: " + $(this).val());
    $("#select-region option").each(function () {
        if ($(this).val() != "") {
            $(this).remove();
        }
    })
    callAjaxRegionListByCountryCode(this.value);
})
function callAjaxRegionListByCountryCode(paramCountryCode) {
    $.ajax({
        url: "http://localhost:8080/countries/code/"+ paramCountryCode+ "/regions",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            console.log(res);
            res.forEach(element => {
                $("#select-region").append($("<option>", {
                    value: element.regionCode,
                    text: element.regionName
                }));
            });
        },
        error: function (err) {
            alert("Lấy danh sách Country thất bại", err.responseText)
        }
    })
}
//Hàm yêu cầu Country list từ sever
function callAjaxCountryList() {
    $.ajax({
        url: "http://localhost:8080/countries",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (res) {
            console.log(res);
            res.forEach(element => {
                $("#select-country").append($("<option>", {
                    value: element.countryCode,
                    text: element.countryName
                }));
            });
        },
        error: function (err) {
            alert("Lấy danh sách Country thất bại", err.responseText)
        }
    })
}