"use strict"
const gBASE_URL = "http://localhost:8080/vouchers"
var gNameCol = ["id", "maVoucher", "phanTramGiamGia","ghiChu","ngayTao","ngayCapNhat","action"];
var gVoucherList = [];
var gVoucherId = "";
const gCOL_ID = 0;
const gCOL_VOUCHER = 1;
const gCOL_DISCOUNT = 2;
const gCOL_NOTE = 3;
const gCOL_CREATED_AT = 4;
const gCOL_UPDATED_AT = 5;
const gCOL_ACTION = 6;
var gTableVoucher = $("#table-voucher").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_VOUCHER] },
    { data: gNameCol[gCOL_DISCOUNT] },
    { data: gNameCol[gCOL_NOTE] },
    { data: gNameCol[gCOL_CREATED_AT] },
    { data: gNameCol[gCOL_UPDATED_AT] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllVoucher();
})
$("#btn-add-voucher").on("click", function () {
  $("#add-voucher-modal").modal("show");
})
$("#btn-confirm-add-voucher").on("click", function () {
  onBtnConfirmAddVoucher()
})
$("#table-voucher tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableVoucher.row(vRowClick).data();
  // console.log(vRowData);
  gVoucherId = vRowData.id;
  $("#edit-voucher-modal").modal("show");
  getVoucherById(gVoucherId);
})
$("#btn-confirm-edit-voucher").on("click", function () {
  onBtnConfirmEditVoucher()
})
$("#table-voucher tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableVoucher.row(vRowClick).data();
  console.log(vRowData);
  gVoucherId = vRowData.id;
  $("#delete-voucher-modal").modal("show");
})
$("#btn-confirm-delete-voucher").on("click", function () {
  onBtnConfirmDeleteVoucher()
})
function getAllVoucher() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gVoucherList = res;
      loadDataToTable(gVoucherList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pVoucherList) {
  "use strict"
  gTableVoucher.clear();
  gTableVoucher.rows.add(pVoucherList);
  gTableVoucher.draw();
}
//Hàm xư lý nút xác nhận thêm Voucher
function onBtnConfirmAddVoucher() {
  let vVoucherObj = {
    maVoucher: "",
    phanTramGiamGia: "",
    ghiChu: ""
  }
  getDataByFormAddModal(vVoucherObj);
  var vCheck = validateData(vVoucherObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vVoucherObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Thêm Voucher thành công");
        getAllVoucher();
        loadDataToTable(gVoucherList);
        $("#add-voucher-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Voucher thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Voucher
function onBtnConfirmEditVoucher() {
  let vVoucherObj = {
    maVoucher: "",
    phanTramGiamGia: "",
    ghiChu: ""
  }
  getDataByFormEditModal(vVoucherObj);
  var vCheck = validateData(vVoucherObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gVoucherId,
      type: "PUT",
      data: JSON.stringify(vVoucherObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa Voucher thành công");
        getAllVoucher();
        loadDataToTable(gVoucherList);
        $("#edit-voucher-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Voucher thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Voucher
function onBtnConfirmDeleteVoucher() {
  $.ajax({
    url: gBASE_URL + "/" + gVoucherId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Voucher thành công");
      getAllVoucher();
      loadDataToTable(gVoucherList);
      $("#delete-voucher-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Voucher thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.maVoucher = $("#inp-voucher-edit-modal").val();
  paramObj.phanTramGiamGia = $("#inp-discount-edit-modal").val();
  paramObj.ghiChu = $("#inp-note-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.maVoucher = $("#inp-voucher-add-modal").val();
  paramObj.phanTramGiamGia = $("#inp-discount-add-modal").val();
  paramObj.ghiChu = $("#inp-note-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.maVoucher == "") {
    alert("Chưa nhập mã giảm giá!");
    vResult = false;
  }
  else if (paramObj.phanTramGiamGia == "") {
    alert("Chưa nhập phần trăm giảm giá!");
    vResult = false;
  }
  return vResult;
}
function getVoucherById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-voucher-edit-modal").val(res.maVoucher);
      $("#inp-discount-edit-modal").val(res.phanTramGiamGia);
      $("#inp-note-edit-modal").val(res.ghiChu);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}