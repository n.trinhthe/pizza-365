"use strict"
const gBASE_URL = "http://localhost:8080/users"
var gNameCol = ["id", "fullname", "email","address","phone","createdAt","updatedAt","action"];
var gUserList = [];
var gUserId = "";
const gCOL_ID = 0;
const gCOL_FULLNAME = 1;
const gCOL_EMAIL= 2;
const gCOL_ADDRESS = 3;
const gCOL_PHONE = 4;
const gCOL_CREATED_AT = 5;
const gCOL_UPDATED_AT = 6;
const gCOL_ACTION = 7;
var gTableUser = $("#table-user").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_FULLNAME] },
    { data: gNameCol[gCOL_EMAIL] },
    { data: gNameCol[gCOL_ADDRESS] },
    { data: gNameCol[gCOL_PHONE] },
    { data: gNameCol[gCOL_CREATED_AT] },
    { data: gNameCol[gCOL_UPDATED_AT] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_CREATED_AT,
      render: function(data){
        if(data!=null){
          let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
        }
      }
    },
    {
      targets: gCOL_UPDATED_AT,
      render: function(data){
        if(data!=null){
          let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
        }
      }
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllUser();
  $.fn.dataTable.ext.errMode = 'none';
})
$("#btn-add-user").on("click", function () {
  $("#add-user-modal").modal("show");
  $("#add-user-modal").find("input").val("")
})
$("#btn-confirm-add-user").on("click", function () {
  onBtnConfirmAddUser()
})
$("#table-user tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableUser.row(vRowClick).data();
  // console.log(vRowData);
  gUserId = vRowData.id;
  $("#edit-user-modal").modal("show");
  getUserById(gUserId);
})
$("#btn-confirm-edit-user").on("click", function () {
  onBtnConfirmEditUser()
})
$("#table-user tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableUser.row(vRowClick).data();
  console.log(vRowData);
  gUserId = vRowData.id;
  $("#delete-user-modal").modal("show");
})
$("#btn-confirm-delete-user").on("click", function () {
  onBtnConfirmDeleteUser()
})
function getAllUser() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gUserList = res;
      loadDataToTable(gUserList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pUserList) {
  "use strict"
  gTableUser.clear();
  gTableUser.rows.add(pUserList);
  gTableUser.draw();
}
//Hàm xư lý nút xác nhận thêm User
function onBtnConfirmAddUser() {
  let vUserObj = {
    fullname: "",
    email: "",
    phone: "",
    address: "",
  }
  getDataByFormAddModal(vUserObj);
  var vCheck = validateData(vUserObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vUserObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Thêm User thành công");
        getAllUser();
        loadDataToTable(gUserList);
        $("#add-user-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm User thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa User
function onBtnConfirmEditUser() {
  let vUserObj = {
    fullname: "",
    email: "",
    phone: "",
    address: "",
  }
  getDataByFormEditModal(vUserObj);
  var vCheck = validateData(vUserObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gUserId,
      type: "PUT",
      data: JSON.stringify(vUserObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa User thành công");
        getAllUser();
        loadDataToTable(gUserList);
        $("#edit-user-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa User thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete User
function onBtnConfirmDeleteUser() {
  $.ajax({
    url: gBASE_URL + "/" + gUserId,
    type: "DELETE",
    success: function () {
      console.log("Xóa User thành công");
      getAllUser();
      loadDataToTable(gUserList);
      $("#delete-user-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa User thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.fullname = $("#inp-fullname-edit-modal").val();
  paramObj.email = $("#inp-email-edit-modal").val();
  paramObj.phone = $("#inp-phone-edit-modal").val();
  paramObj.address = $("#inp-address-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.fullname = $("#inp-fullname-add-modal").val();
  paramObj.email = $("#inp-email-add-modal").val();
  paramObj.phone = $("#inp-phone-add-modal").val();
  paramObj.address = $("#inp-address-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.fullname == "") {
    alert("Chưa nhập họ và tên!");
    vResult = false;
  }
  else if (paramObj.email == "") {
    alert("Chưa nhập địa chỉ email!");
    vResult = false;
  }
  else if (paramObj.address == "") {
    alert("Chưa nhập địa chỉ!");
    vResult = false;
  }
  else if (paramObj.phone == "") {
    alert("Chưa nhập số điện thoại!");
    vResult = false;
  }
  return vResult;
}
function getUserById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-fullname-edit-modal").val(res.fullname);
      $("#inp-email-edit-modal").val(res.email);
      $("#inp-phone-edit-modal").val(res.address);
      $("#inp-address-edit-modal").val(res.phone);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}