"use strict"
const gBASE_URL = "http://localhost:8080/drinks"
var gNameCol = ["id", "tenNuocUong", "maNuocUong","donGia","createdAt","updatedAt","action"];
var gDrinkList = [];
var gDrinkId = "";
const gCOL_ID = 0;
const gCOL_DRINK = 1;
const gCOL_DRINK_CODE = 2;
const gCOL_PRICE = 3;
const gCOL_CREATED_AT = 4;
const gCOL_UPDATED_AT = 5;
const gCOL_ACTION = 6;
var gTableDrink = $("#table-drink").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_DRINK] },
    { data: gNameCol[gCOL_DRINK_CODE] },
    { data: gNameCol[gCOL_PRICE] },
    { data: gNameCol[gCOL_CREATED_AT] },
    { data: gNameCol[gCOL_UPDATED_AT] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_CREATED_AT,
      render: function(data){
        let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
      }
    },
    {
      targets: gCOL_UPDATED_AT,
      render: function(data){
        let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
      }
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllDrink();
})
$("#btn-add-drink").on("click", function () {
  $("#add-drink-modal").modal("show");
})
$("#btn-confirm-add-drink").on("click", function () {
  onBtnConfirmAddDrink()
})
$("#table-drink tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableDrink.row(vRowClick).data();
  // console.log(vRowData);
  gDrinkId = vRowData.id;
  $("#edit-drink-modal").modal("show");
  getDrinkById(gDrinkId);
})
$("#btn-confirm-edit-drink").on("click", function () {
  onBtnConfirmEditDrink()
})
$("#table-drink tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableDrink.row(vRowClick).data();
  console.log(vRowData);
  gDrinkId = vRowData.id;
  $("#delete-drink-modal").modal("show");
})
$("#btn-confirm-delete-drink").on("click", function () {
  onBtnConfirmDeleteDrink()
})
function getAllDrink() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gDrinkList = res;
      loadDataToTable(gDrinkList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pDrinkList) {
  "use strict"
  gTableDrink.clear();
  gTableDrink.rows.add(pDrinkList);
  gTableDrink.draw();
}
//Hàm xư lý nút xác nhận thêm Drink
function onBtnConfirmAddDrink() {
  let vDrinkObj = {
    tenNuocUong: "",
    maNuocUong: "",
    donGia: ""
  }
  getDataByFormAddModal(vDrinkObj);
  var vCheck = validateData(vDrinkObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vDrinkObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Thêm Drink thành công");
        getAllDrink();
        loadDataToTable(gDrinkList);
        $("#add-drink-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Drink thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Drink
function onBtnConfirmEditDrink() {
  let vDrinkObj = {
    tenNuocUong: "",
    maNuocUong: "",
    donGia: ""
  }
  getDataByFormEditModal(vDrinkObj);
  var vCheck = validateData(vDrinkObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gDrinkId,
      type: "PUT",
      data: JSON.stringify(vDrinkObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa Drink thành công");
        getAllDrink();
        loadDataToTable(gDrinkList);
        $("#edit-drink-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Drink thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Drink
function onBtnConfirmDeleteDrink() {
  $.ajax({
    url: gBASE_URL + "/" + gDrinkId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Drink thành công");
      getAllDrink();
      loadDataToTable(gDrinkList);
      $("#delete-drink-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Drink thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.tenNuocUong = $("#inp-ten-nuoc-uong-edit-modal").val();
  paramObj.maNuocUong = $("#inp-ma-nuoc-uong-edit-modal").val();
  paramObj.donGia = $("#inp-price-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.tenNuocUong = $("#inp-ten-nuoc-uong-add-modal").val();
  paramObj.maNuocUong = $("#inp-ma-nuoc-uong-add-modal").val();
  paramObj.donGia = $("#inp-price-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.tenNuocUong == "") {
    alert("Chưa nhập tên nước uống!");
    vResult = false;
  }
  else if (paramObj.maNuocUong == "") {
    alert("Chưa nhập mã nước uống!");
    vResult = false;
  }
  else if (paramObj.donGia == "") {
    alert("Chưa nhập đơn giá!");
    vResult = false;
  }
  return vResult;
}
function getDrinkById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-ten-nuoc-uong-edit-modal").val(res.tenNuocUong);
      $("#inp-ma-nuoc-uong-edit-modal").val(res.maNuocUong);
      $("#inp-price-edit-modal").val(res.donGia);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}