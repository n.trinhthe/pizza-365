"use strict"
const gBASE_URL = "http://localhost:8080/countries"
var gNameCol = ["id", "countryName", "countryCode","regionTotal","action"];
var gCountryList = [];
var gCountryId = "";
var gSTT = 1;
const gCOL_ID = 0;
const gCOL_COUNTRY = 1;
const gCOL_COUNTRY_CODE = 2;
const gCOL_REGION_TOTAL = 3;
const gCOL_ACTION = 4;
var gTableCountry = $("#table-country").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_COUNTRY] },
    { data: gNameCol[gCOL_COUNTRY_CODE] },
    { data: gNameCol[gCOL_REGION_TOTAL] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_ID,
      render: function(){
        return gSTT++;
      },
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllCountry();
})
$("#btn-add-country").on("click", function () {
  $("#add-country-modal").modal("show");
  $("#add-country-modal").find("input").val("");
})
$("#btn-confirm-add-country").on("click", function () {
  onBtnConfirmAddCountry()
})
$("#table-country tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableCountry.row(vRowClick).data();
  // console.log(vRowData);
  gCountryId = vRowData.id;
  $("#edit-country-modal").modal("show");
  getCountryById(gCountryId);
})
$("#btn-confirm-edit-country").on("click", function () {
  onBtnConfirmEditCountry()
})
$("#table-country tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableCountry.row(vRowClick).data();
  console.log(vRowData);
  gCountryId = vRowData.id;
  $("#delete-country-modal").modal("show");
})
$("#btn-confirm-delete-country").on("click", function () {
  onBtnConfirmDeleteCountry()
})
function getAllCountry() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gCountryList = res;
      loadDataToTable(gCountryList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pCountryList) {
  "use strict"
  gTableCountry.clear();
  gTableCountry.rows.add(pCountryList);
  gTableCountry.draw();
}
//Hàm xư lý nút xác nhận thêm Country
function onBtnConfirmAddCountry() {
  let vCountryObj = {
    countryName: "",
    countryCode: ""
  }
  getDataByFormAddModal(vCountryObj);
  var vCheck = validateData(vCountryObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vCountryObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      async: false,
      success: function (res) {
        console.log("Thêm Country thành công");
        gSTT = 1;
        getAllCountry();
        loadDataToTable(gCountryList);
        $("#add-country-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Country thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Country
function onBtnConfirmEditCountry() {
  let vCountryObj = {
    countryName: "",
    countryCode: ""
  }
  getDataByFormEditModal(vCountryObj);
  var vCheck = validateData(vCountryObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gCountryId,
      type: "PUT",
      data: JSON.stringify(vCountryObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa Country thành công");
        getAllCountry();
        loadDataToTable(gCountryList);
        $("#edit-country-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Country thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Country
function onBtnConfirmDeleteCountry() {
  $.ajax({
    url: gBASE_URL + "/" + gCountryId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Country thành công");
      getAllCountry();
      loadDataToTable(gCountryList);
      $("#delete-country-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Country thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.countryName = $("#inp-country-edit-modal").val();
  paramObj.countryCode = $("#inp-country-code-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.countryName = $("#inp-country-add-modal").val();
  paramObj.countryCode = $("#inp-country-code-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.countryName == "") {
    alert("Chưa nhập tên quốc gia!");
    vResult = false;
  }
  else if (paramObj.countryCode == "") {
    alert("Chưa nhập mã quốc gia!");
    vResult = false;
  }
  return vResult;
}
function getCountryById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-country-edit-modal").val(res.countryName);
      $("#inp-country-code-edit-modal").val(res.countryCode);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}