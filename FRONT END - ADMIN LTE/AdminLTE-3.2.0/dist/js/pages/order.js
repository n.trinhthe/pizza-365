"use strict"
const gBASE_URL = "http://localhost:8080/orders"
var gNameCol = ["id", "orderCode", "pizzaSize","pizzaType","voucherCode","price","paid","createdAt","updatedAt","action"];
var gOrderList = [];
var gOrderId = "";
const gCOL_ID = 0;
const gCOL_ORDER_CODE = 1;
const gCOL_PIZZA_SIZE= 2;
const gCOL_PIZZA_TYPE = 3;
const gCOL_VOUCHER_CODE = 4;
const gCOL_PRICE = 5;
const gCOL_PAID = 6;
const gCOL_CREATED_AT = 7;
const gCOL_UPDATED_AT = 8;
const gCOL_ACTION = 9;
var gTableOrder = $("#table-order").DataTable({
  columns: [
    { data: gNameCol[gCOL_ID] },
    { data: gNameCol[gCOL_ORDER_CODE] },
    { data: gNameCol[gCOL_PIZZA_SIZE] },
    { data: gNameCol[gCOL_PIZZA_TYPE] },
    { data: gNameCol[gCOL_VOUCHER_CODE] },
    { data: gNameCol[gCOL_PRICE] },
    { data: gNameCol[gCOL_PAID] },
    { data: gNameCol[gCOL_CREATED_AT] },
    { data: gNameCol[gCOL_UPDATED_AT] },
    { data: gNameCol[gCOL_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOL_ACTION,
      defaultContent: `<i class="far fa-edit btn-edit text-primary fa-xl"></i> &nbsp <i class="fas fa-trash-alt btn-delete text-danger fa-xl"></i>`,
    },
    {
      targets: gCOL_CREATED_AT,
      render: function(data){
        if(data!=null){
          let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
        }
      }
    },
    {
      targets: gCOL_UPDATED_AT,
      render: function(data){
        if(data!=null){
          let date = new Date(data);
        return date.toLocaleDateString("vi-VN");
        }
      }
    }
  ],
  autoWidth: true
})
$(document).ready(function () {
  getAllOrder();
  $.fn.dataTable.ext.errMode = 'none';
})
$("#btn-add-order").on("click", function () {
  $("#add-order-modal").modal("show");
  $("#add-order-modal").find("input").val("")
})
$("#btn-confirm-add-order").on("click", function () {
  onBtnConfirmAddOrder()
})
$("#table-order tbody").on("click", ".btn-edit", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableOrder.row(vRowClick).data();
  // console.log(vRowData);
  gOrderId = vRowData.id;
  $("#edit-order-modal").modal("show");
  getOrderById(gOrderId);
})
$("#btn-confirm-edit-order").on("click", function () {
  onBtnConfirmEditOrder()
})
$("#table-order tbody").on("click", ".btn-delete", function () {
  let vRowClick = $(this).closest("tr");
  let vRowData = gTableOrder.row(vRowClick).data();
  console.log(vRowData);
  gOrderId = vRowData.id;
  $("#delete-order-modal").modal("show");
})
$("#btn-confirm-delete-order").on("click", function () {
  onBtnConfirmDeleteOrder()
})
function getAllOrder() {
  "use strict"
  $.ajax({
    url: gBASE_URL,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      gOrderList = res;
      loadDataToTable(gOrderList);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}
function loadDataToTable(pOrderList) {
  "use strict"
  gTableOrder.clear();
  gTableOrder.rows.add(pOrderList);
  gTableOrder.draw();
}
//Hàm xư lý nút xác nhận thêm Order
function onBtnConfirmAddOrder() {
  let vOrderObj = {
    orderCode: "",
    pizzaSize: "",
    pizzaType: "",
    voucherCode: "",
    price: "",
    paid: ""
  }
  getDataByFormAddModal(vOrderObj);
  var vCheck = validateData(vOrderObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL,
      type: "POST",
      data: JSON.stringify(vOrderObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Thêm Order thành công");
        getAllOrder();
        loadDataToTable(gOrderList);
        $("#add-order-modal").modal("hide");
      },
      error: function (err) {
        alert("Thêm Order thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận chỉnh sửa Order
function onBtnConfirmEditOrder() {
  let vOrderObj = {
    orderCode: "",
    pizzaSize: "",
    pizzaType: "",
    voucherCode: "",
    price: "",
    paid: ""
  }
  getDataByFormEditModal(vOrderObj);
  var vCheck = validateData(vOrderObj);
  if (vCheck == true) {
    $.ajax({
      url: gBASE_URL + "/" + gOrderId,
      type: "PUT",
      data: JSON.stringify(vOrderObj),
      contentType: "application/json;charset=utf8",
      dataType: "json",
      success: function (res) {
        console.log("Chỉnh sửa Order thành công");
        getAllOrder();
        loadDataToTable(gOrderList);
        $("#edit-order-modal").modal("hide");
      },
      error: function (err) {
        alert("Chỉnh sửa Order thất bại" + err.responseText);
      }
    })
  }
}
//Hàm xử lý nút xác nhận delete Order
function onBtnConfirmDeleteOrder() {
  $.ajax({
    url: gBASE_URL + "/" + gOrderId,
    type: "DELETE",
    success: function () {
      console.log("Xóa Order thành công");
      getAllOrder();
      loadDataToTable(gOrderList);
      $("#delete-order-modal").modal("hide");
    },
    error: function (err) {
      alert("Xóa Order thất bại" + err.responseText);
    }
  })
}
//Hàm truy xuất dữ liệu form Edit Modal
function getDataByFormEditModal(paramObj) {
  paramObj.orderCode = $("#inp-order-code-edit-modal").val();
  paramObj.pizzaSize = $("#inp-pizza-size-edit-modal").val();
  paramObj.pizzaType = $("#inp-pizza-type-edit-modal").val();
  paramObj.voucherCode = $("#inp-voucher-edit-modal").val();
  paramObj.price = $("#inp-price-edit-modal").val();
  paramObj.paid = $("#inp-paid-edit-modal").val();
}
//Hàm truy xuất dữ liệu form Add Modal
function getDataByFormAddModal(paramObj) {
  paramObj.orderCode = $("#inp-order-code-add-modal").val();
  paramObj.pizzaSize = $("#inp-pizza-size-add-modal").val();
  paramObj.pizzaType = $("#inp-pizza-type-add-modal").val();
  paramObj.voucherCode = $("#inp-voucher-add-modal").val();
  paramObj.price = $("#inp-price-add-modal").val();
  paramObj.paid = $("#inp-paid-add-modal").val();
}

function validateData(paramObj) {
  let vResult = true;
  if (paramObj.orderCode == "") {
    alert("Chưa nhập Order Code!");
    vResult = false;
  }
  else if (paramObj.pizzaSize == "") {
    alert("Chưa nhập Pizza Size!");
    vResult = false;
  }
  else if (paramObj.pizzaType == "") {
    alert("Chưa nhập Pizza Type!");
    vResult = false;
  }
  else if (paramObj.price == "") {
    alert("Chưa nhập giá!");
    vResult = false;
  }
  return vResult;
}
function getOrderById(paramId) {
  "use strict"
  $.ajax({
    url: gBASE_URL + "/" + paramId,
    dataType: "json",
    type: "GET",
    success: function (res) {
      console.log(res);
      $("#inp-order-code-edit-modal").val(res.orderCode);
      $("#inp-pizza-size-edit-modal").val(res.pizzaSize);
      $("#inp-pizza-type-edit-modal").val(res.pizzaType);
      $("#inp-voucher-edit-modal").val(res.voucherCode);
      $("#inp-price-edit-modal").val(res.price);
      $("#inp-paid-edit-modal").val(res.paid);
    },
    error: function (err) {
      alert(err.responseText);
    }
  })
}