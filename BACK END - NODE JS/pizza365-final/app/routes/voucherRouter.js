const express = require('express');
const {createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getVoucherOfOrder} = require('../controllers/voucherController');
const voucherRouter = express.Router()

voucherRouter.post('/vouchers',createVoucher);
voucherRouter.get('/vouchers',getAllVoucher);
voucherRouter.get('/vouchers/:voucherId',getVoucherById);
voucherRouter.put('/vouchers/:voucherId',updateVoucherById);
voucherRouter.delete('/vouchers/:voucherId',deleteVoucherById);
voucherRouter.get([
'/devcamp-pizza365/vouchers/',
'/devcamp-pizza365/vouchers/:voucherId']
,getVoucherOfOrder);

module.exports = voucherRouter;