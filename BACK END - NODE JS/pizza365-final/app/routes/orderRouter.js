const express = require('express');
const {createOrder, getAllOrderOfUser, getOrderById, updateOrderById, deleteOrderById, createOrdersOfUser} = require('../controllers/orderController');
const orderRouter = express.Router();

orderRouter.post('/users/:userId/orders', createOrder);
orderRouter.get('/users/:userId/orders', getAllOrderOfUser);
orderRouter.get('/orders/:orderId', getOrderById);
orderRouter.put('/orders/:orderId', updateOrderById);
orderRouter.delete('/users/:userId/orders/:orderId', deleteOrderById);
orderRouter.post('/devcamp-pizza365/orders',createOrdersOfUser);
module.exports = orderRouter;