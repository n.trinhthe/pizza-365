const express = require("express");
const {createDrink, getAllDrink, getDrinkById, updateDrinkById, deleteDrinkById, getDrinkList } = require('../controllers/drinkController');
const drinkRouter = express.Router();

drinkRouter.post('/drinks',createDrink);
drinkRouter.get('/drinks',getAllDrink);
drinkRouter.get('/drinks/:drinkId',getDrinkById);
drinkRouter.put('/drinks/:drinkId',updateDrinkById);
drinkRouter.delete('/drinks/:drinkId',deleteDrinkById);
drinkRouter.get('/devcamp-pizza365/drinks',getDrinkList);
module.exports = drinkRouter;