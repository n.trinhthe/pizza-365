const express = require('express');
const userRouter = express.Router();
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById, getAllLimitUser, getAllSkipUser, getAllSortUser, getAllSkipLimitUser, getAllSortSkipLimitUser } = require('../controllers/userController')

userRouter.post('/users',createUser);
userRouter.get('/users',getAllUser);
userRouter.get('/limit-users',getAllLimitUser);
userRouter.get('/skip-users',getAllSkipUser);
userRouter.get('/sort-users',getAllSortUser);
userRouter.get('/skip-limit-users',getAllSkipLimitUser);
userRouter.get('/sort-skip-limit-users',getAllSortSkipLimitUser);
userRouter.get('/users/:userId',getUserById);
userRouter.put('/users/:userId',updateUserById);
userRouter.delete('/users/:userId',deleteUserById);

module.exports = userRouter;