const mongoose = require('mongoose');
const VoucherDB = mongoose.Schema;
const voucherSchema = new VoucherDB({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        unique: true,
        require: true,
    },
    phanTramGiamGia: {
        type: Number,
        require: true
    },
    ghiChu: {
        type: String,
        require: false
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
})
module.exports = mongoose.model('Voucher',voucherSchema)