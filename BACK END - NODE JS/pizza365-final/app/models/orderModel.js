const mongoose = require('mongoose');
const OrderDB = mongoose.Schema;
const orderSchema = new OrderDB({
    _id: mongoose.Types.ObjectId,
    orderCode: {
        type: String,
        unique: true
    },
    pizzaSize: {
        type: String,
        require: true
    },
    pizzaType: {
        type: String,
        require: true
    },
    voucher: {
            type: mongoose.Types.ObjectId,
            ref: 'Voucher'
    },
    drink: {
            type: mongoose.Types.ObjectId,
            ref: 'Drink'
    },
    status: {
        type: String,
        require: true,
        default: 'open'
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('Order',orderSchema);