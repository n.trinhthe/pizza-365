const mongoose = require('mongoose');
const UserDB = mongoose.Schema;
const userSchema = new UserDB({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    address: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
        unique: true
    },
    order: [
       {
        type: mongoose.Types.ObjectId,
        ref: "Order"
       } 
    ],
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model('User',userSchema);