const mongoose = require('mongoose');
const DrinkDB = mongoose.Schema;
const drinkSchema = new DrinkDB({
    _id: mongoose.Schema.Types.ObjectId,
    maNuocUong : {
       type: String,
       unique: true,
       require: true
    },
    tenNuocUong: {
        type: String,
        require: true
    },
    donGia: {
        type: Number,
        require: true
    },
    ngayTao: {
        type: Date,
        default: Date.now()
    },
    ngayCapNhat: {
        type: Date,
        default: Date.now()
    }
});
module.exports = mongoose.model('Drink', drinkSchema)

    