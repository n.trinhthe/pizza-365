const mongoose = require('mongoose');
const userModel = require('../models/userModel');
//CREATE USER
const createUser = (req,res)=>{
    let body = req.body;
    let  newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone,
    })
    userModel.create(newUser,(error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(201).json({
                message: "CREATE User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL USER
const  getAllUser = (req,res) => {
    userModel.find((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL LIMIT USER
const  getAllLimitUser = (req,res) => {
    //B1: Chuẩn bị dữ liệu
    let  limitUser = req.query.limitUser;
    let condition = {};
    if (limitUser){
        condition = limitUser
    }
    userModel.find()
    .limit(condition)
    .exec((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All Limit User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL SKIP USER
const  getAllSkipUser = (req,res) => {
    //B1: Chuẩn bị dữ liệu
    let  skipUser = req.query.skipUser;
    let condition = {};
    if (skipUser){
        condition = skipUser
    }
    userModel.find()
    .skip(condition)
    .exec((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All Skip User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL SORT USER
const  getAllSortUser = (req,res) => {
    //B1: Chuẩn bị dữ liệu
    let  sortUser = req.query.sortUser;
    let condition = {};
    if (sortUser){
        condition = sortUser
    }
    userModel.find()
    .sort(`field ${condition}`)
    .exec((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All Sorst User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL LIMIT SKIP USER
const  getAllSkipLimitUser = (req,res) => {
    //B1: Chuẩn bị dữ liệu
    let  skipUser = req.query.skipUser;
    let  limitUser = req.query.limitUser;
    let condition = {};
    if (skipUser){
        condition.skipUser = skipUser
    }
    if(limitUser){
        condition.limitUser = limitUser
    }
    userModel.find()
    .skip(condition.skipUser)
    .limit(condition.limitUser)
    .exec((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All Limit Skip User Successfully!",
                users: data
            })
        }
    })
}
//GET ALL LIMIT SKIP USER
const  getAllSortSkipLimitUser = (req,res) => {
    //B1: Chuẩn bị dữ liệu
    let  skipUser = req.query.skipUser;
    let  limitUser = req.query.limitUser;
    let  sortUser = req.query.sortUser;
    let condition = {};
    if (skipUser){
        condition.skipUser = skipUser
    }
    if(limitUser){
        condition.limitUser = limitUser
    }
    if(sortUser){
        condition.sortUser = limitUser
    }
    userModel.find()
    .skip(condition.skipUser)
    .limit(condition.limitUser)
    .sort(`field ${condition.sortUser}`)
    .exec((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET All Sort Limit Skip User Successfully!",
                users: data
            })
        }
    })
}
//GET USER BY ID
const getUserById = (req,res) => {
    let id = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "ID User Not Valid" + error.message
        }) 
    }
    
    userModel.findById(id,(error, data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: `GET User By ID ${id} Successfully!`,
                users: data
            })
        }
    })
}
//UPDATE USER
const updateUserById = (req,res) => {
    let id = req.params.userId;
    let body = req.body;
    let  newUser = new userModel({
        fullName: body.fullName,
        email: body.email,
        address: body.address,
        phone: body.phone
    });

    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "ID User Not Valid" + error.message
        }) 
    }
    
    userModel.findByIdAndUpdate(id, newUser, (error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE User By ID ${id} Successfully!`,
                users: data
            })
        }
    })

}
//DELETE USER
const deleteUserById = (req,res) => {
    let id = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message: "ID User Not Valid" + error.message
        })
    }
    userModel.findByIdAndDelete(id, (error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE User By ID ${id} Successfully!`,
                users: data
            })
        }
    })
}
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById,
    getAllLimitUser,
    getAllSkipUser,
    getAllSortUser,
    getAllSkipLimitUser,
    getAllSortSkipLimitUser
}