const mongoose = require('mongoose');
const voucherModel = require('../models/voucherModel');

//CREATE VOUCHER
const createVoucher = (req, res) => {
    console.log("CREATE Voucher");
    let body = req.body;
    let newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    })
    voucherModel.create(newVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(201).json({
                message: "Create Voucher Successfully!",
                vouchers: data
            })
        }
    })
}

//GET ALL VOUCHER 
const getAllVoucher = (req, res) => {
    console.log("GET All Voucher");
    voucherModel.find((error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "GET Voucher Successfully!",
                vouchers: data
            })
        }
    })
}
//GET Voucher By ID
const getVoucherById = (req, res) => {
    console.log("GET Voucher By ID");
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(400).json({
            message: "ID Voucher Not Valid" + error.message
        })
    }
    voucherModel.findById(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json(data)
        }
    })
}

//UPDATE VOUCHER
const updateVoucherById = (req, res) => {
    console.log("UPDATE Voucher");
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(400).json({
            message: "ID Voucher Not Valid" + error.message
        })
    }
    let body = req.body;
    let newVoucher = new voucherModel({
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    })
    voucherModel.findByIdAndUpdate(id, newVoucher, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "UPDATE Voucher Successfully!",
                vouchers: data
            })
        }
    })
}
//DELETE VOUCHER
const deleteVoucherById = (req, res) => {
    console.log("DELETE Voucher");
    let id = req.params.voucherId;
    if (!mongoose.Types.ObjectId(id)) {
        return res.status(400).json({
            message: "ID Voucher Not Valid" + error.message
        })
    }
    voucherModel.findByIdAndDelete(id, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "DELETE Voucher Successfully!",
                vouchers: data
            })
        }
    })
}
//GET Voucher By Order
const getVoucherOfOrder = (req, res) => {
    let voucherId = req.params.voucherId;
    voucherModel.findOne({ maVoucher: voucherId }).exec((errFindVoucher, dataFindVoucher) => {
        if (errFindVoucher) {
            return res.status(500).json({
                message: "Internal Server Error" + errFindVoucher.message
            })
        }
        else {
            if (!dataFindVoucher) {
                return res.status(404).send();
            } else
                return res.status(200).json(dataFindVoucher)
        }
    })
}

module.exports = { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById, getVoucherOfOrder }