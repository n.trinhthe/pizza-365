const mongoose = require('mongoose');
const orderModel = require('../models/orderModel');
const userModel = require("../models/userModel");
const voucherModel = require('../models/voucherModel');
const drinkModel = require('../models/drinkModel');

//CREATE ORDER
const createOrder = (req, res) => {
    let userId = req.params.userId;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is invalid"
        })
    }
    let newOrder = new orderModel({
        _id: mongoose.Types.ObjectId(),
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status
    })
    orderModel.create(newOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            userModel.findByIdAndUpdate(userId,
                {
                    $push: { order: data._id }
                },

                (err, updateUser) => {
                    if (err) {
                        return res.status(500).json({
                            message: "Internal Server Error" + error.message
                        })
                    }
                    else {
                        return res.status(201).json({
                            message: `CREATE Order Successfully!`,
                            orders: data
                        })
                    }
                }
            )
        }
    });
}
//GET ALL ORDER
const getAllOrderOfUser = (req, res) => {
    let userId = req.params.userId;
    userModel.findById(userId)
        .populate('order')
        .exec((error, data) => {
            if (error) {
                return res.status(500).json({
                    message: "Internal Server Error" + error.message
                })
            }
            else {
                return res.status(200).json({
                    message: `GET All Order Successfully!`,
                    orders: data.order
                })
            }
        })
}
//GET ORDER BY ID
const getOrderById = (req, res) => {
    let orderId = req.params.orderId;
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is invalid"
        })
    }
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: `GET Order Successfully!`,
                orders: data
            })
        }
    })
}
//UPDATE ORDER
const updateOrderById = (req, res) => {
    let orderId = req.params.orderId;
    let body = req.body;
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is invalid"
        })
    }
    let newOrder = new orderModel({
        orderCode: body.orderCode,
        pizzaSize: body.pizzaSize,
        pizzaType: body.pizzaType,
        voucher: body.voucher,
        drink: body.drink,
        status: body.status
    })
    orderModel.findByIdAndUpdate(orderId, newOrder, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: `Update Order Successfully!`,
                orders: data
            })
        }
    })
}
const deleteOrderById = (req, res) => {
    let userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is invalid"
        })
    }
    let orderId = req.params.orderId;
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is invalid"
        })
    }
    orderModel.findByIdAndDelete(orderId, (error, data) => {
        if (error) {
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            userModel.findByIdAndUpdate(userId,
                {
                    $pull: { order: orderId }
                },
                (error, data) => {
                    if (error) {
                        return res.status(500).json({
                            message: "Internal Server Error" + error.message
                        })
                    }
                    else {
                        return res.status(204).json({
                            message: "Delete Order Successfully!"
                        })
                    }
                })
        }
    })
}
//CREATE ORDER BY USER
const createOrdersOfUser = (req, res) => {
    let body = req.body;
    let orderCode = Math.random().toString(36).slice(-8);
    drinkModel.findOne({ maNuocUong: body.maNuocUong }, (errFindDrink, dataFindDrink) => {
        if (errFindDrink) {
            return res.status(500).json({
                message: "Internal Server Error" + errFindDrink.message
            })
        }
        else {
            userModel.findOne({ email: body.email }).exec((errFindUser, dataFindUser) => {
                if (errFindUser) {
                    return res.status(500).json({
                        message: "Internal Server Error" + errFindUser.message
                    })
                }
                else {
                    //Không tìm thấy user
                    if (!dataFindUser) {
                        userModel.create({
                            _id: mongoose.Types.ObjectId(),
                            fullName: body.fullName,
                            email: body.email,
                            address: body.address,
                            phone: body.phone,
                        }, (errCreatedUser, dataCreatedUser) => {
                            if (errCreatedUser) {
                                return res.status(500).json({
                                    message: "Internal Server Error" + errCreatedUser.message
                                })
                            }
                            else {
                                voucherModel.findOne({ maVoucher: body.maVoucher }, (errFindVoucher, dataFindVoucher) => {
                                    if (!dataFindVoucher) {
                                        orderModel.create({
                                            _id: mongoose.Types.ObjectId(),
                                            orderCode: orderCode,
                                            pizzaSize: body.pizzaSize,
                                            pizzaType: body.pizzaType,
                                            drink: dataFindDrink
                                        }, (errCreatedOrder, dataCreatedOrder) => {
                                            if (errCreatedOrder) {
                                                return res.status(500).json({
                                                    message: "Internal Server Error" + errCreatedOrder.message
                                                })
                                            }
                                            else {
                                                return res.status(200).json({
                                                    _id: dataCreatedOrder._id,
                                                    orderCode: orderCode,
                                                    pizzaSize: dataCreatedOrder.pizzaSize,
                                                    pizzaType: dataCreatedOrder.pizzaType,
                                                    kichCo: body.kichCo,
                                                    duongKinh: body.duongKinh,
                                                    suon: body.suon,
                                                    salad: body.salad,
                                                    soLuongNuoc: body.soLuongNuoc,
                                                    thanhTien: body.thanhTien,
                                                    voucher: body.voucher,
                                                    discount: 0,
                                                    drink: findDrink.maNuocUong,
                                                    fullName: dataCreatedUser.fullName,
                                                    email: dataCreatedUser.email,
                                                    address: dataCreatedUser.address,
                                                    phone: dataCreatedUser.phone,
                                                    loiNhan: body.loiNhan,
                                                    ngayTao: dataCreatedOrder.ngayTao,
                                                    ngayCapNhat: dataCreatedOrder.ngayCapNhat
                                                })
                                            }
                                        })
                                    }
                                    else {
                                        orderModel.create({
                                            _id: mongoose.Types.ObjectId(),
                                            orderCode: orderCode,
                                            pizzaSize: body.pizzaSize,
                                            pizzaType: body.pizzaType,
                                            voucher: dataFindVoucher,
                                            drink: dataFindDrink
                                        }, (errCreatedOrder, dataCreatedOrder) => {
                                            if (errCreatedOrder) {
                                                return res.status(500).json({
                                                    message: "Internal Server Error" + errCreatedOrder.message
                                                })
                                            }
                                            else {
                                                return res.status(200).json({
                                                    _id: dataCreatedOrder._id,
                                                    orderCode: orderCode,
                                                    pizzaSize: dataCreatedOrder.pizzaSize,
                                                    pizzaType: dataCreatedOrder.pizzaType,
                                                    kichCo: body.kichCo,
                                                    duongKinh: body.duongKinh,
                                                    suon: body.suon,
                                                    salad: body.salad,
                                                    soLuongNuoc: body.soLuongNuoc,
                                                    thanhTien: body.thanhTien,
                                                    voucher: dataFindVoucher.maVoucher,
                                                    discount: dataFindVoucher.phanTramGiamGia,
                                                    drink: findDrink.maNuocUong,
                                                    fullName: dataCreatedUser.fullName,
                                                    email: dataCreatedUser.email,
                                                    address: dataCreatedUser.address,
                                                    phone: dataCreatedUser.phone,
                                                    loiNhan: body.loiNhan,
                                                    ngayTao: dataCreatedOrder.ngayTao,
                                                    ngayCapNhat: dataCreatedOrder.ngayCapNhat
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                    //Nếu thấy tim thấy User
                    else {
                        voucherModel.findOne({ maVoucher: body.maVoucher }, (errFindVoucher, dataFindVoucher) => {
                            //Không tìm thấy Voucher
                            if (!dataFindVoucher) {
                                orderModel.create({
                                    _id: mongoose.Types.ObjectId(),
                                    orderCode: orderCode,
                                    pizzaSize: body.pizzaSize,
                                    pizzaType: body.pizzaType,
                                    drink: dataFindDrink
                                }, (errCreatedOrder, dataCreatedOrder) => {
                                    if (errCreatedOrder) {
                                        return res.status(500).json({
                                            message: "Internal Server Error" + errCreatedOrder.message
                                        })
                                    }
                                    else {
                                        return res.status(200).json({
                                            _id: dataCreatedOrder._id,
                                            orderCode: orderCode,
                                            pizzaSize: dataCreatedOrder.pizzaSize,
                                            pizzaType: dataCreatedOrder.pizzaType,
                                            kichCo: body.kichCo,
                                            duongKinh: body.duongKinh,
                                            suon: body.suon,
                                            salad: body.salad,
                                            soLuongNuoc: body.soLuongNuoc,
                                            thanhTien: body.thanhTien,
                                            voucher: body.voucher,
                                            discount: 0,
                                            drink: dataFindDrink.maNuocUong,
                                            fullName: dataFindUser.fullName,
                                            email: dataFindUser.email,
                                            address: dataFindUser.address,
                                            phone: dataFindUser.phone,
                                            loiNhan: body.loiNhan,
                                            ngayTao: dataCreatedOrder.ngayTao,
                                            ngayCapNhat: dataCreatedOrder.ngayCapNhat
                                        })
                                    }
                                })
                            }
                            //Tìm thấy voucher
                            else {
                                orderModel.create({
                                    _id: mongoose.Types.ObjectId(),
                                    orderCode: orderCode,
                                    pizzaSize: body.pizzaSize,
                                    pizzaType: body.pizzaType,
                                    voucher: dataFindVoucher,
                                    drink: dataFindDrink
                                }, (errCreatedOrder, dataCreatedOrder) => {
                                    if (errCreatedOrder) {
                                        return res.status(500).json({
                                            message: "Internal Server Error" + errCreatedOrder.message
                                        })
                                    }
                                    else {
                                        return res.status(200).json({
                                            _id: dataCreatedOrder._id,
                                            orderCode: orderCode,
                                            pizzaSize: dataCreatedOrder.pizzaSize,
                                            pizzaType: dataCreatedOrder.pizzaType,
                                            kichCo: body.kichCo,
                                            duongKinh: body.duongKinh,
                                            suon: body.suon,
                                            salad: body.salad,
                                            soLuongNuoc: body.soLuongNuoc,
                                            thanhTien: body.thanhTien,
                                            voucher: dataFindVoucher.maVoucher,
                                            discount: dataFindVoucher.phanTramGiamGia,
                                            drink: dataFindDrink.maNuocUong,
                                            fullName: dataFindUser.fullName,
                                            email: dataFindUser.email,
                                            address: dataFindUser.address,
                                            phone: dataFindUser.phone,
                                            loiNhan: body.loiNhan,
                                            ngayTao: dataCreatedOrder.ngayTao,
                                            ngayCapNhat: dataCreatedOrder.ngayCapNhat
                                        })
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
    })
}
                  
module.exports = {
    createOrder,
    getAllOrderOfUser,
    getOrderById,
    updateOrderById,
    deleteOrderById,
    createOrdersOfUser
}