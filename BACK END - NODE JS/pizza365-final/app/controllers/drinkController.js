const mongoose = require('mongoose')
const drinkModel = require ('../models/drinkModel');

//CREATE DRINK
const createDrink = (req,res) => {
    console.log("CREATE Drink ");
    let body = req.body;
    let newDrink = new drinkModel({
        _id: mongoose.Types.ObjectId(),
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    })
    drinkModel.create (newDrink,(error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(201).json({
                message: "Create Drink Successfully!",
                drinks: data
            })
        }
    })
}   
//GET ALL DRINK
const getAllDrink = (req,res) => {
    console.log("GET All Drink");
    drinkModel.find((error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(201).json({
                message: "Get All Drink Successfully!",
                drinks: data
            })
        }
    })
}
//GET DRINK BY ID
const getDrinkById = (req,res) =>{
    console.log("GET Drink by ID");
    let id = req.params.drinkId;
    if(!mongoose.Types.ObjectId(id)){
        return res.status(400).json({
            message: "ID Drink Not Valid" + error.message
        }) 
    }
    drinkModel.findById(id,(error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "Get Drink Successfully!",
                drinks: data
            })
        }
    })
}
//UPDATE DRINK BY ID

const updateDrinkById = (req,res) => {
    console.log("UPDATE Drink")
    let id = req.params.drinkId;
    let body = req.body;
    if(!mongoose.Types.ObjectId(id)){
        return res.status(400).json({
            message: "ID Drink Not Valid" + error.message
        }) 
    }
    if(isNaN(body.donGia)){
        return res.status(400).json({
            message: "donGia" + error.message
        }) 
    }
    let newDrink = drinkModel({
        maNuocUong: body.maNuocUong,
        tenNuocUong: body.tenNuocUong,
        donGia: body.donGia
    })
    drinkModel.findByIdAndUpdate(id,newDrink,(error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "Update Drink Successfully!",
                drinks: data
            })
        }
    })
}
//DELETE DRINK
const deleteDrinkById = (req,res) => {
    console.log("DELETE Drink");
    let id = req.params.drinkId;
    if(!mongoose.Types.ObjectId(id)){
        return res.status(400).json({
            message: "ID Drink Not Valid" + error.message
        }) 
    }
    drinkModel.findByIdAndDelete(id,(error,data)=>{
        if (error){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {
            return res.status(200).json({
                message: "Delete Drink Successfully!",
                drinks: data
            })
        }
    })
}
const getDrinkList = (req,res)=>{
    drinkModel.find((err,data)=>{
        if (err){
            return res.status(500).json({
                message: "Internal Server Error" + error.message
            })
        }
        else {  
            let drinkList = [];
            data.forEach((drink)=> {drinkList.push(drink)});
            return res.status(200).json(drinkList);
        }
    })
}

module.exports = {createDrink, getAllDrink, getDrinkById,updateDrinkById, deleteDrinkById,getDrinkList}