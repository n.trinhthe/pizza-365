const express  = require ("express");
const path = require("path")
const app = express();
var mongoose = require('mongoose');
const drinkModel = require('./app/models/drinkModel');
const voucherModel = require('./app/models/voucherModel');
const orderModel = require('./app/models/orderModel');
const userModel = require('./app/models/userModel');
const drinkRouter = require('./app/routes/drinkRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const userRouter = require('./app/routes/userRouter');
const orderRouter = require('./app/routes/orderRouter');

const port = 8000;
//Connect Mongoose
// mongoose.connect('mongodb+srv://admin:admin123@cluster0.q8qnwvl.mongodb.net/CRUD_Pizza365?retryWrites=true&w=majority', function(error) {
//     if (error) throw error;
//     console.log('Successfully connected MongoDB');
//    });
//Middleware
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH');
    next();
});
// Lỗi không thể kết nối MongoDB trên MongoDB Compass local host  
mongoose.connect('mongodb://0.0.0.0:27017/CRUD_Pizza365?directConnection=true', function(error) {
    if (error) throw error;
    console.log('Successfully connected MongoDB');
   })
app.get("/",(req,res)=>{
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/pizza365.v1.9.html"));
})
app.use(express.static(__dirname + "/views"));
app.use(express.json());
//Use Router
app.use(drinkRouter);
app.use(voucherRouter);
app.use(userRouter);
app.use(orderRouter);

app.listen (port,() => {
    console.log("App listening on port: ",port)
});