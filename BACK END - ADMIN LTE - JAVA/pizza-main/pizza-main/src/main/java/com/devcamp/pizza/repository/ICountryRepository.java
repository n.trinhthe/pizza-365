package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.Country;

public interface ICountryRepository extends JpaRepository<Country,Long> {
    Optional<Country> findById(Long id);
    Country findByCountryCode(String countryCode);
}
