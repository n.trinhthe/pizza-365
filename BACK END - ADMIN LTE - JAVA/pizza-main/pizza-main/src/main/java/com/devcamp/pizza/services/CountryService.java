package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.Country;
import com.devcamp.pizza.repository.ICountryRepository;



@Service
public class CountryService {
    @Autowired
    ICountryRepository countryRepository;

    public List<Country> getAllCountry() {
        ArrayList<Country> countryList = new ArrayList<>();
        countryRepository.findAll().forEach(countryList::add);
        return countryList;
    }

    public List<Country> getAllCountryAndPage(String page, String size) {
        Pageable sizeOfPage = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size),Sort.by("countryName").ascending());
        ArrayList<Country> countryList = new ArrayList<>();
        countryRepository.findAll(sizeOfPage).forEach(countryList::add);
        return countryList;
    }

    public Country getCountryById(long countryId) {
        Optional<Country> countryData = countryRepository.findById(countryId);
        if (countryData.isPresent()) {
            return countryData.get();
        } else {
            return null;
        }
    }

    public Country createCountry(Country pCountry) {
        Country newCountry = new Country();
        newCountry.setCountryCode(pCountry.getCountryCode());
        newCountry.setCountryName(pCountry.getCountryName());
        countryRepository.save(newCountry);
        return newCountry;
    }

    public Country updateCountry(Country pCountry, long countryId) {
        Optional<Country> countryData = countryRepository.findById(countryId);
        if (countryData.isPresent()) {
            Country updateCountry = countryData.get();
            updateCountry.setCountryCode(pCountry.getCountryCode());
            updateCountry.setCountryName(pCountry.getCountryName());
            updateCountry.setRegions(pCountry.getRegions());
            countryRepository.save(updateCountry);
            return updateCountry;
        }
        else {
            return null;
        }
    }

    public void deleteCountryById(long countryId){
        countryRepository.deleteById(countryId);
    }

    public long countCountry(){
        return countryRepository.count();
    }

    public Country getCountryByCode(String countryCode) {
        Country foundCountry = countryRepository.findByCountryCode(countryCode);
        if (foundCountry!=null) {
            return foundCountry;
        } else {
            return null;
        }
    }
}
