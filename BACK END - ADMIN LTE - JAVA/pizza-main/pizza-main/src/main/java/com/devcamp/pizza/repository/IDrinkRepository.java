package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.Drink;

public interface IDrinkRepository extends JpaRepository<Drink,Long> {
    Optional<Drink> findById(Long id);
}
