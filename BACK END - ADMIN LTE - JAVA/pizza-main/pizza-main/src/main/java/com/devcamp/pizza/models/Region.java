package com.devcamp.pizza.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "region")
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Region Code Not Empty")
    @NotBlank(message = "Region Code Not Blank")
    @Column(name = "region_code")
    private String regionCode;

    @NotEmpty(message = "Region Name Not Empty")
    @Column(name = "region_name")
    private String regionName;
    
    @ManyToOne
    @JsonIgnore
    private Country country;

    @Transient
    private String countryName;

    @Transient
    private String countryCode;
    public Region() {
    }
    
    public Region(String regionCode, String regionName, Country country) {
        this.regionCode = regionCode;
        this.regionName = regionName;
        this.country = country;
    }

    public String getCountryName() {
        return getCountry().getCountryName();
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    @JsonIgnore
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country cCountry) {
        this.country = cCountry;
    }

    public String getCountryCode() {
        return getCountry().getCountryCode();
    }

    
}
