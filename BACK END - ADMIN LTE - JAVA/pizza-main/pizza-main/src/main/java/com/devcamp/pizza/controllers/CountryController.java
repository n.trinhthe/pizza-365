package com.devcamp.pizza.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.Country;
import com.devcamp.pizza.services.CountryService;

@RestController
@CrossOrigin
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping("/countries")
    public ResponseEntity<List<Country>> getAllCountry(){
        try {
            return new ResponseEntity<>(countryService.getAllCountry(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/countries",params = {"page", "size"})
    public ResponseEntity<List<Country>> getAllCountryAndPage(
        @RequestParam(value = "page",defaultValue = "0") String page,
        @RequestParam(value = "size",defaultValue = "5") String size){
        try {
            return new ResponseEntity<>(countryService.getAllCountryAndPage(page,size), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/{id}")
    public ResponseEntity<Country> getCountryById(@PathVariable("id") long countryId) {
        Country findCountry = countryService.getCountryById(countryId);
        if (findCountry != null) {
            try {
                return new ResponseEntity<>(findCountry, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/countries")
    public ResponseEntity<Country> createCountry(@RequestBody Country pCountry ) {
        try {
            Country createdCountry = countryService.createCountry(pCountry);
            return new ResponseEntity<>(createdCountry, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            // return ResponseEntity.unprocessableEntity()
            //         .body("Tạo country thất bại: " + ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/countries/{id}")
    public ResponseEntity<Object> getCountryById(@PathVariable("id") long countryId, @RequestBody Country pCountry) {
        Country updatedCountry = countryService.updateCountry(pCountry, countryId);
        if (updatedCountry != null) {
            try {
                return new ResponseEntity<>(updatedCountry, HttpStatus.OK);
            } catch (Exception ex) {
                // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
                return ResponseEntity.unprocessableEntity()
                        .body("Update country thất bại: " + ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/countries/{id}")
    public ResponseEntity<Country> deleteCountryById(@PathVariable("id") long countryId) {
        try {
            countryService.deleteCountryById(countryId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/count")
    public ResponseEntity<Object> countCountry() {
        try {
            return new ResponseEntity<>(countryService.countCountry(),HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/code/{code}")
    public ResponseEntity<Country> getCountryById(@PathVariable("code") String code) {
        Country findCountry = countryService.getCountryByCode(code);
        if (findCountry != null) {
            try {
                return new ResponseEntity<>(findCountry, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }



}
