package com.devcamp.pizza.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.Region;
import com.devcamp.pizza.services.RegionService;


@RestController
@CrossOrigin
public class RegionController {
    @Autowired
    RegionService regionService;

    @GetMapping("/regions")
    public ResponseEntity<List<Region>> getAllRegion() {
        List<Region> regionList = regionService.getAllRegion();
        try {
            return new ResponseEntity<>(regionList,HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/{id}/regions")
    public ResponseEntity<Object> getRegionsByCountryId(@PathVariable("id") Long countryId) {
        List<Region> listRegion = regionService.getRegionsByCountryId(countryId);
        if (listRegion != null) {
            try {
                return new ResponseEntity<>(listRegion, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/regions/{id}")
    public ResponseEntity<Region> getRegionsById(@PathVariable("id") Long regionId) {
        Region findRegion = regionService.getRegionsById(regionId);
        if (findRegion != null) {
            try {
                return new ResponseEntity<>(findRegion, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/countries/{countryId}/regions")
    public ResponseEntity<Object> getRegionById(@RequestBody Region pRegion,
            @PathVariable("countryId") Long countryId) {
        Region createdRegion = regionService.createRegion(pRegion, countryId);
        try {
            return new ResponseEntity<>(createdRegion, HttpStatus.CREATED);
        } catch (Exception ex) {
            // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Tạo country thất bại(REGION): " + ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/countries/{countryId}/regions/{regionId}")
    public ResponseEntity<Object> updateRegionById(@RequestBody Region pRegion,
            @PathVariable("regionId") Long regionId) {
        Region updatedRegion = regionService.updateRegion(pRegion, regionId);
        if (updatedRegion != null) {
            try {
                return new ResponseEntity<>(updatedRegion, HttpStatus.OK);
            } catch (Exception ex) {
                // return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
                return ResponseEntity.unprocessableEntity()
                        .body("Update country thất bại: " + ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/regions/{regionId}")
    public ResponseEntity<Object> deleteRegionById(@PathVariable("regionId") Long regionId) {
        try {
            regionService.deleteRegion(regionId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/code/{countryCode}/regions")
    public ResponseEntity<List<Region>> getRegionsByCountryCode(@PathVariable("countryCode") String countryCode) {
        List<Region> listRegion = regionService.findRegionsByCountryCode(countryCode);
        if (listRegion != null) {
            try {
                return new ResponseEntity<>(listRegion, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/regions/check/{regionId}")
    public ResponseEntity<Boolean> checkRegionById(@PathVariable("regionId") Long regionId) {
        try {
            return new ResponseEntity<>(regionService.checkRegionById(regionId),HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/countries/{id}/regions/count")
    public ResponseEntity<Long> getCountryById(@PathVariable("id") long countryId) {
        Long countRegion = regionService.countRegionByCountryId(countryId);
        if (countRegion != -1) {
            try {
                return new ResponseEntity<>(countRegion, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
