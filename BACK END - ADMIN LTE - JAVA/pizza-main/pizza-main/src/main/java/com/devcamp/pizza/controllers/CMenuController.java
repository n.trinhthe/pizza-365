package com.devcamp.pizza.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.CMenu;
import com.devcamp.pizza.services.CMenuService;

@RestController
@CrossOrigin
public class CMenuController {
    @Autowired
    CMenuService cMenuService;

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getAllCMenu() {
        try {
            return new ResponseEntity<>(cMenuService.getAllCMenu(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/menu/{id}")
    public ResponseEntity<CMenu> getMenuById(@PathVariable("id") Long id) {
        CMenu findMenu = cMenuService.getCMenuById(id);
        if (findMenu != null) {
            try {
                return new ResponseEntity<>(findMenu, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/menu")
    public ResponseEntity<CMenu> createMenu(@RequestBody CMenu pMenu) {
        try {
            return new ResponseEntity<>(cMenuService.createMenu(pMenu), HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/menu/{id}")
    public ResponseEntity<CMenu> getMenuById(@PathVariable("id") Long id, @RequestBody CMenu pMenu) {
        CMenu updatedMenu = cMenuService.updateMenu(pMenu, id);
        if (updatedMenu != null) {
            try {
                return new ResponseEntity<>(updatedMenu, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/menu/{id}")
    public ResponseEntity<CMenu> deleteMenu(@PathVariable("id") Long id) {
        try {
            cMenuService.deleteMenu(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
