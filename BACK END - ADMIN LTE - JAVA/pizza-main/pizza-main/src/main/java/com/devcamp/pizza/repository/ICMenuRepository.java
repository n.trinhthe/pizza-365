package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.CMenu;

public interface ICMenuRepository extends JpaRepository<CMenu,Long>{
    Optional<CMenu> findById(Long id);
}
