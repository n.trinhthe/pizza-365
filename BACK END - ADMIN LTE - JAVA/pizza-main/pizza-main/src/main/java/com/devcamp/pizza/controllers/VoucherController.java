package com.devcamp.pizza.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.Voucher;
import com.devcamp.pizza.services.VoucherService;

@RestController
@CrossOrigin
public class VoucherController {
    @Autowired
    VoucherService voucherService;

    @GetMapping("/vouchers")
    public ResponseEntity<List<Voucher>> getAllVoucher() {
        try {
            return new ResponseEntity<>(voucherService.getAllVoucher(), HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> getVoucherById(@PathVariable("id") Long id){
        Voucher findVoucher = voucherService.getVoucherById(id);
        if (findVoucher!=null){
            try {
                return new ResponseEntity<>(findVoucher, HttpStatus.OK);
            }
            catch (Exception ex){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/vouchers")
    public ResponseEntity<Voucher> createVoucher(@RequestBody Voucher pVoucher) {
        Voucher createdVoucher = voucherService.createVoucher(pVoucher);
        try {
            return new ResponseEntity<>(createdVoucher, HttpStatus.CREATED);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> updateVoucherById(@RequestBody Voucher pVoucher, @PathVariable("id") Long id){
        Voucher updatedVoucher = voucherService.updateVoucher(pVoucher,id);
        if (updatedVoucher!=null){
            try {
                return new ResponseEntity<>(updatedVoucher, HttpStatus.OK);
            }
            catch (Exception ex){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/vouchers/{id}")
    public ResponseEntity<Voucher> deleteVoucherById(@PathVariable("id") Long id) {
        try {
            voucherService.deleteVoucher(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
