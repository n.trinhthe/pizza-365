package com.devcamp.pizza.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.Drink;
import com.devcamp.pizza.services.DrinkService;

@RestController
@CrossOrigin
public class DrinkController {
    @Autowired
    DrinkService drinkService;

    @GetMapping("/drinks")
    public ResponseEntity<List<Drink>> getAllDrink(){
        try {
            return  new ResponseEntity<>(drinkService.getAllDrink(),HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<Drink> getAllDrink(@PathVariable("id") Long id){
        Drink findDrink = drinkService.getDrinkById(id);
        if (findDrink != null){
            try {
                return  new ResponseEntity<>(findDrink,HttpStatus.OK);
            }
            catch (Exception ex){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Drink> createDrink(@RequestBody Drink pDrink){
        Drink createdDrink = drinkService.createDrink(pDrink);
        try {
            return  new ResponseEntity<>(createdDrink,HttpStatus.CREATED);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Drink> updateDrink(@RequestBody Drink pDrink, @PathVariable("id") Long id){
        Drink updatedDrink = drinkService.getDrinkById(id);
        if (updatedDrink != null){
            try {
                return  new ResponseEntity<>(updatedDrink,HttpStatus.OK);
            }
            catch (Exception ex){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<Drink> deleteDrink(@PathVariable("id") Long id){
        try {
            drinkService.deleteDrink(id);
            return  new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
