package com.devcamp.pizza.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name="voucher")
public class Voucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name="phan_tram_giam_gia")
    @NotEmpty
    private String phanTramGiamGia;

    @Column(name="ghi_chu")
    private String ghiChu;

    @Column(name="ma_voucher")
    @NotEmpty
    private String maVoucher;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name="created_at", nullable = true, updatable = false)
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name="updated_at", nullable = true, updatable = true)
    private Date ngayCapNhat;
    
    public Voucher() {
    }
    public Voucher(long id, String phanTramGiamGia, String ghiChu, String maVoucher, Date ngayTao, Date ngayCapNhat) {
        super();
        this.id = id;
        this.phanTramGiamGia = phanTramGiamGia;
        this.ghiChu = ghiChu;
        this.maVoucher = maVoucher;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getPhanTramGiamGia() {
        return phanTramGiamGia;
    }
    public void setPhanTramGiamGia(String phanTramGiamGia) {
        this.phanTramGiamGia = phanTramGiamGia;
    }
    public String getGhiChu() {
        return ghiChu;
    }
    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }
    public String getMaVoucher() {
        return maVoucher;
    }
    public void setMaVoucher(String maVoucher) {
        this.maVoucher = maVoucher;
    }
    public Date getNgayTao() {
        return ngayTao;
    }
    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }
    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }
    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    
}
