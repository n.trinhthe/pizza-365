package com.devcamp.pizza.controllers;

import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.Order;
import com.devcamp.pizza.services.OrderService;


@RestController
@CrossOrigin
public class OrderController {
    @Autowired
    OrderService orderService;

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders() {
        try {
            return new ResponseEntity<>(orderService.getAllOrders(), HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{userId}/orders")
    public ResponseEntity<Set<Order>> getOrdersByUserId(@PathVariable("userId") Long userId) {
        Set<Order> orderList = orderService.getOrdersByUserId(userId);
        if (orderList != null) {
            try {
                return new ResponseEntity<>(orderList, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/orders/{orderId}")
    public ResponseEntity<Order> getOrderById(@PathVariable("orderId") Long orderId) {
        Order findOrder = orderService.getOrderById(orderId);
        if (findOrder != null) {
            try {
                return new ResponseEntity<>(findOrder, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping("/orders")
    public ResponseEntity<Object> createOrder(@Valid @RequestBody Order pOrder) {
        try {
            return new ResponseEntity<>(orderService.createOrder(pOrder), HttpStatus.OK);
        } catch (Exception ex) {
            return ResponseEntity.unprocessableEntity()
                    .body("Create order failed: " + ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/orders/{orderId}")
    public ResponseEntity<Object> updateOrder(@RequestBody Order pOrder, @PathVariable("orderId") Long orderId) {
        Order updatedOrder = orderService.updateOrder(pOrder, orderId);
        if (updatedOrder != null) {
            try {
                return new ResponseEntity<>(updatedOrder, HttpStatus.OK);
            } catch (Exception ex) {
                // return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                return ResponseEntity.unprocessableEntity()
                        .body("Update failed Order: " + ex.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/orders/{orderId}")
    public ResponseEntity<Object> updateOrder(@PathVariable("orderId") Long orderId) {
        try {
            orderService.deleteOrder(orderId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
