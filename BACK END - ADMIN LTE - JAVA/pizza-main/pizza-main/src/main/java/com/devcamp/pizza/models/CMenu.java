package com.devcamp.pizza.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

@Entity
@Table(name = "menu")
public class CMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "kich_co")

    private String kichCo;

    @Column(name = "duong_kinh")

    private String duongKinh;

    @Column(name = "suon")

    private int suon;

    @Column(name = "salad")

    private String salad;

    @Column(name = "so_luong_nuoc")

    private int soLuongNuoc;

    @Column(name = "price")

    private double price;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "created_at", nullable = false, updatable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedBy
    @Column(name = "updated_at", nullable = true, updatable = true)
    private Date updatedAt;

    public CMenu() {
    }

    public CMenu(String kichCo, String duongKinh, int suon, String salad,
            int soLuongNuoc) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuoc = soLuongNuoc;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKichCo() {
        return kichCo;
    }

    public void setKichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public String getSalad() {
        return salad;
    }

    public void setSalad(String salad) {
        this.salad = salad;
    }

    public int getSoLuongNuoc() {
        return soLuongNuoc;
    }

    public void setSoLuongNuoc(int soLuongNuoc) {
        this.soLuongNuoc = soLuongNuoc;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

}
