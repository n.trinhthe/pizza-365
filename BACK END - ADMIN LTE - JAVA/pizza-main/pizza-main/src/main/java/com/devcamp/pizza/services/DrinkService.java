package com.devcamp.pizza.services;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.Drink;
import com.devcamp.pizza.repository.IDrinkRepository;

@Service
public class DrinkService {
    @Autowired
    IDrinkRepository drinkRepository;

    public List<Drink> getAllDrink() {
        ArrayList<Drink> drinkList = new ArrayList<>();
        drinkRepository.findAll().forEach(drinkList::add);
        return drinkList;
    }

    public Drink getDrinkById(Long id) {
        Optional<Drink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()) {
            return drinkData.get();
        } else {
            return null;
        }
    }

    public Drink createDrink(Drink pDrink) {
        Drink newDrink = new Drink();
        newDrink.setTenNuocUong(pDrink.getTenNuocUong());
        newDrink.setMaNuocUong(pDrink.getMaNuocUong());
        newDrink.setDonGia(pDrink.getDonGia());
        newDrink.setGhiChu(pDrink.getGhiChu());
        newDrink.setCreatedAt(new Date());
        drinkRepository.save(newDrink);
        return newDrink;
    }

    public Drink updateDrink(Drink pDrink, Long id) {
        Optional<Drink> drinkData = drinkRepository.findById(id);
        if (drinkData.isPresent()) {
            Drink updatedDrink = drinkData.get();
            updatedDrink.setTenNuocUong(pDrink.getTenNuocUong());
            updatedDrink.setMaNuocUong(pDrink.getMaNuocUong());
            updatedDrink.setDonGia(pDrink.getDonGia());
            updatedDrink.setGhiChu(pDrink.getGhiChu());
            updatedDrink.setUpdatedAt(new Date());
            drinkRepository.save(updatedDrink);
            return updatedDrink;
        } else {
            return null;
        }
    }

    public void deleteDrink(Long id){
        drinkRepository.deleteById(id);
    }
}
