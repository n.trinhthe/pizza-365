package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.Voucher;

public interface IVoucherRepository extends JpaRepository<Voucher,Long> {
    Optional<Voucher> findById(Long id);
}
