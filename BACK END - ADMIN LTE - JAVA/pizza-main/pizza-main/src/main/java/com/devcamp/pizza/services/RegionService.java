package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.Country;
import com.devcamp.pizza.models.Region;
import com.devcamp.pizza.repository.ICountryRepository;
import com.devcamp.pizza.repository.IRegionRepository;



@Service
public class RegionService {
    @Autowired
    IRegionRepository regionRepository;
    @Autowired
    ICountryRepository countryRepository;

    public List<Region> getAllRegion() {
        ArrayList<Region> regionList = new ArrayList<>();
        regionRepository.findAll().forEach(regionList::add);
        return regionList;
    }

    public List<Region> getRegionsByCountryId(Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }

    public Region getRegionsById(Long regionId) {
        Optional<Region> findRegion = regionRepository.findById(regionId);
        if (findRegion != null) {
            return findRegion.get();
        } else {
            return null;
        }
    }

    public Region createRegion(Region pRegion, Long countryId) {
        Optional<Country> countryData = countryRepository.findById(countryId);
        if (countryData.isPresent()) {
            Region createdRegion = new Region();
            createdRegion.setRegionCode(pRegion.getRegionCode());
            createdRegion.setRegionName(pRegion.getRegionName());
            createdRegion.setCountry(countryData.get());
            regionRepository.save(createdRegion);
            return createdRegion;
        }
        else {
            return null;
        }
    }

    public Region updateRegion(Region pRegion, Long regionId){
        Optional<Region> regionData = regionRepository.findById(regionId);
        if(regionData.isPresent()){
            Region updatedRegion = regionData.get();
            updatedRegion.setRegionCode(pRegion.getRegionCode());
            updatedRegion.setRegionName(pRegion.getRegionName());
            regionRepository.save(updatedRegion);
            return updatedRegion;
        }
        else {
            return null;
        }
    }

    public void deleteRegion(Long regionId){
        regionRepository.deleteById(regionId);
    }

    public List<Region> findRegionsByCountryCode(String countryCode){
        Country findCountry = countryRepository.findByCountryCode(countryCode);
        if(findCountry!=null){
            return findCountry.getRegions();
        }
        else {
            return null;
        }
    }

    public boolean checkRegionById(Long regionId){
        return regionRepository.existsById(regionId);
    }

    public long countRegionByCountryId(Long countryId){
        Optional<Country> countryData = countryRepository.findById(countryId);
        if(countryData.isPresent()){
            return countryData.get().getRegionTotal();
        }
        else {
            return -1;
        }
    }
}
