package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.Order;
import com.devcamp.pizza.models.User;
import com.devcamp.pizza.repository.IOrderRepository;
import com.devcamp.pizza.repository.IUserRepository;


@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;
    @Autowired
    IUserRepository userRepository;

    public List<Order> getAllOrders() {
        ArrayList<Order> orderList = new ArrayList<>();
        orderRepository.findAll().forEach(orderList::add);
        return orderList;
    }

    public Set<Order> getOrdersByUserId(Long userId) {
        Optional<User> userData = userRepository.findById(userId);
        if (userData.isPresent()) {
            User user = userData.get();
            return user.getOrders();
        } else {
            return null;
        }
    }

    public Order getOrderById(Long orderId){
        Optional<Order> orderData = orderRepository.findById(orderId);
        if (orderData.isPresent()){
            return orderData.get();
        }
        else {
            return null;
        }
    }

    public Order createOrder(Order pOrder) {
        Order newOrder = new Order();
        newOrder.setOrderCode(pOrder.getOrderCode());
        newOrder.setPizzaSize(pOrder.getPizzaSize());
        newOrder.setPizzaType(pOrder.getPizzaType());
        newOrder.setVoucherCode(pOrder.getVoucherCode());
        newOrder.setPrice(pOrder.getPrice());
        newOrder.setPaid(pOrder.getPaid());
        newOrder.setCreatedAt(new Date());
        orderRepository.save(newOrder);
        return newOrder;
    }

    public Order updateOrder(Order pOrder, Long orderId) {
        Optional<Order> orderData = orderRepository.findById(orderId);
        if (orderData.isPresent()) {
            Order updatedOrder = orderData.get();
            updatedOrder.setOrderCode(pOrder.getOrderCode());
            updatedOrder.setPizzaSize(pOrder.getPizzaSize());
            updatedOrder.setPizzaType(pOrder.getPizzaType());
            updatedOrder.setVoucherCode(pOrder.getVoucherCode());
            updatedOrder.setPrice(pOrder.getPrice());
            updatedOrder.setPaid(pOrder.getPaid());
            updatedOrder.setUpdatedAt(new Date());
            orderRepository.save(updatedOrder);
            return updatedOrder;
        } else {
            return null;
        }
    }

    public void deleteOrder(Long orderId){
        orderRepository.deleteById(orderId);
    }
}
