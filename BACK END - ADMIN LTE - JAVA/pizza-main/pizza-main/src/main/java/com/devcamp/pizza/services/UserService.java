package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.security.auth.message.callback.PrivateKeyCallback.Request;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.User;
import com.devcamp.pizza.repository.IUserRepository;

@Service
public class UserService {
    @Autowired
    IUserRepository userRepository;
    public List<User> getAllUsers(String page, String size){
        Pageable sizeOfPage = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size), Sort.by("id").ascending());
        ArrayList<User> userList = new ArrayList<>();
        userRepository.findAll(sizeOfPage).forEach(userList::add);
        return userList;
    }

    public User getUserById(Long userId){
        Optional<User> userData = userRepository.findById(userId);
        if (userData.isPresent()){
            return userData.get();
        }
        else {
            return null;
        }
    }

    public User createUser(User pUser){
        User newUser = new User();
        newUser.setFullname(pUser.getFullname());
        newUser.setEmail(pUser.getEmail());
        newUser.setPhone(pUser.getPhone());
        newUser.setAddress(pUser.getAddress());
        newUser.setCreatedAt(new Date());
        newUser.setOrders(pUser.getOrders());
        userRepository.save(newUser);
        return newUser;
    }

    public User updateUser(User pUser, Long userId){
        Optional<User> userData = userRepository.findById(userId);
        if(userData.isPresent()){
            User updateUser = userData.get();
            updateUser.setFullname(pUser.getFullname());
            updateUser.setEmail(pUser.getEmail());
            updateUser.setPhone(pUser.getPhone());
            updateUser.setAddress(pUser.getAddress());
            updateUser.setUpdateAt(new Date());
            userRepository.save(updateUser);
            return updateUser;
        }
        else {
            return null;
        }
    }

    public void deleteUser(Long userId){
        userRepository.deleteById(userId);
    }

    public long countUser(){
        return userRepository.count();
    }

    public Boolean checkUserById(Long id){
        return userRepository.existsById(id);
    }
}
