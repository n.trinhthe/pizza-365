package com.devcamp.pizza.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import java.util.List;

@Entity
@Table(name = "country")
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "country_code", unique = true)
    @NotEmpty(message = "Country Name Not Empty")
    @NotBlank(message = "Country Name Not Blank")
    private String countryCode;

    @Column(name = "country_name")
    @NotEmpty(message = "Country Name Not Empty")
    private String countryName;

    @Transient
    private int regionTotal;

    @OneToMany(targetEntity = Region.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "country_id")
    private List<Region> regions;

    public Country() {
    }

    public Country(
            @NotEmpty(message = "Country Name Not Empty") @NotBlank(message = "Country Name Not Blank") String countryCode,
            @NotEmpty(message = "Country Name Not Empty") String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    
    public int getRegionTotal() {
    if (getRegions()!=null){
    return getRegions().size();
    }
    else {
    return 0;
    }
    }
}
