package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.CMenu;
import com.devcamp.pizza.repository.ICMenuRepository;

@Service
public class CMenuService {
    @Autowired
    ICMenuRepository cMenuRepository;

    public List<CMenu> getAllCMenu() {
        ArrayList<CMenu> menuList = new ArrayList<>();
        cMenuRepository.findAll().forEach(menuList::add);
        return menuList;
    }

    public CMenu getCMenuById(Long id) {
        Optional<CMenu> cMenuData = cMenuRepository.findById(id);
        if (cMenuData.isPresent()) {
            return cMenuData.get();
        } else {
            return null;
        }
    }

    public CMenu createMenu(CMenu pMenu) {
        CMenu newMenu = new CMenu();
        newMenu.setDuongKinh(pMenu.getDuongKinh());
        newMenu.setKichCo(pMenu.getKichCo());
        newMenu.setSalad(pMenu.getSalad());
        newMenu.setSuon(pMenu.getSuon());
        newMenu.setSoLuongNuoc(pMenu.getSoLuongNuoc());
        newMenu.setPrice(pMenu.getPrice());
        newMenu.setCreatedAt(new Date());
        cMenuRepository.save(newMenu);
        return newMenu;
    }

    public CMenu updateMenu(CMenu pMenu, Long id) {
        Optional<CMenu> cMenuData = cMenuRepository.findById(id);
        if (cMenuData.isPresent()) {
            CMenu updatedMenu = cMenuData.get();
            updatedMenu.setDuongKinh(pMenu.getDuongKinh());
            updatedMenu.setKichCo(pMenu.getKichCo());
            updatedMenu.setSalad(pMenu.getSalad());
            updatedMenu.setSuon(pMenu.getSuon());
            updatedMenu.setSoLuongNuoc(pMenu.getSoLuongNuoc());
            updatedMenu.setPrice(pMenu.getPrice());
            updatedMenu.setUpdatedAt(new Date());
            cMenuRepository.save(updatedMenu);
            return updatedMenu;
        } else {
            return null;
        }
    }

    public void deleteMenu(Long id){
        cMenuRepository.deleteById(id);
    }
}
