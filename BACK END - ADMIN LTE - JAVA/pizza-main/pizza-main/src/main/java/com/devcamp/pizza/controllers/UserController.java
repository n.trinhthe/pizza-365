package com.devcamp.pizza.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza.models.User;
import com.devcamp.pizza.services.UserService;



@RestController
@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUser(
        @RequestParam (value = "page",defaultValue = "0") String page,
        @RequestParam (value = "size",defaultValue = "5") String size){
        try {
            return new ResponseEntity<>(userService.getAllUsers(page,size),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable("userId") Long userId){
        if (userService.getUserById(userId)!=null){
            try {
                return new ResponseEntity<>(userService.getUserById(userId),HttpStatus.OK); 
            }
            catch (Exception ex){
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); 
            }
        }
        else {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/users")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User pUser){
        try {
            return new ResponseEntity<>(userService.createUser(pUser),HttpStatus.CREATED);
        }
        catch (Exception ex) {
            // return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity().body("Create User Failed: " + ex.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/users/{userId}")
    public ResponseEntity<Object> updateUser(@RequestBody User pUser, @PathVariable("userId") Long userId){
        User updatedUser = userService.updateUser(pUser,userId);
        if (updatedUser!=null){
            try {
                return new ResponseEntity<>(updatedUser,HttpStatus.OK); 
            }
            catch (Exception ex){
                // return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR); 
                return ResponseEntity.unprocessableEntity().body("Update User fialed: " + ex.getCause().getCause().getMessage());
            }
        }
        else {
            return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/users/{userId}")
    public ResponseEntity<User> deleteUser(@PathVariable("userId") Long userId){
        try {
            userService.deleteUser(userId);
            return new ResponseEntity<>(null,HttpStatus.NO_CONTENT);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/users/count")
    public ResponseEntity<Long> countUser(){
        try {
            return new ResponseEntity<>(userService.countUser(),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/users/{userId}/check")
    public ResponseEntity<Boolean> checkUserById(@PathVariable("userId") Long userId){
        try {
            return new ResponseEntity<>(userService.checkUserById(userId),HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
