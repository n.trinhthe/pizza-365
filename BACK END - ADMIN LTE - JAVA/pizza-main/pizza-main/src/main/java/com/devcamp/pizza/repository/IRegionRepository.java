package com.devcamp.pizza.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.Region;

public interface IRegionRepository extends JpaRepository<Region,Long> {
    List<Region> findByCountryId(Long id);
    Optional<Region> findById(Long id);
}
