-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 04, 2022 lúc 04:27 PM
-- Phiên bản máy phục vụ: 10.4.25-MariaDB
-- Phiên bản PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pizza_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `country`
--

CREATE TABLE `country` (
  `id` bigint(20) NOT NULL,
  `country_code` varchar(255) DEFAULT NULL,
  `country_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `country`
--

INSERT INTO `country` (`id`, `country_code`, `country_name`) VALUES
(1, 'VN', 'Việt Nam'),
(2, 'AU', 'Australia'),
(3, 'USA', 'Hoa Kỳ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `drink`
--

CREATE TABLE `drink` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) NOT NULL,
  `ma_nuoc_uong` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) DEFAULT NULL,
  `ghi_chu` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `drink`
--

INSERT INTO `drink` (`id`, `don_gia`, `ma_nuoc_uong`, `updated_at`, `created_at`, `ten_nuoc_uong`, `ghi_chu`) VALUES
(1, 10000, 'TRATAC', '2022-10-17 21:21:12', '2022-10-17 21:21:12', 'Trà tắc', NULL),
(2, 15000, 'COCA', '2022-10-17 21:21:12', '2022-10-17 21:21:12', 'Cocacola', NULL),
(3, 15000, 'PEPSI', '2022-10-17 21:21:12', '2022-10-17 21:21:12', 'Pepsi', NULL),
(4, 20000, 'REDBULL', '2022-10-29 00:00:00', '2022-10-29 00:00:00', 'Bò húc', 'Redbull Thái'),
(6, 14000, 'TRADAO', '2022-10-29 01:40:24', '2022-10-29 01:23:37', 'Trà đào', ''),
(7, 16000, 'STING', NULL, '2022-10-29 16:58:01', 'Sting Dâu', NULL),
(10, 100000, 'TRADA', '2022-10-29 00:00:00', '2022-10-29 17:37:27', 'Trà đá', ''),
(35, 20000, 'TRASUA', NULL, '2022-11-03 23:03:32', 'Trà sữa', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(99);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `duong_kinh` varchar(255) DEFAULT NULL,
  `kich_co` varchar(255) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `salad` varchar(255) DEFAULT NULL,
  `so_luong_nuoc` int(11) DEFAULT NULL,
  `suon` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`id`, `created_at`, `duong_kinh`, `kich_co`, `price`, `salad`, `so_luong_nuoc`, `suon`, `updated_at`) VALUES
(1, '2022-11-04 10:05:10', '20cm', 'S', 150000, '200gam', 2, 4, '2022-11-04 16:05:10'),
(2, '2022-11-04 16:06:04', '25cm', 'M', 200000, '300gam', 3, 6, NULL),
(3, '2022-11-04 10:06:52', '30cm', 'L', 250000, '400gam', 4, 8, '2022-11-04 17:03:24'),
(95, '2022-11-04 17:00:27', '35cm', 'XL', 300000, '400gam', 5, 10, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order`
--

CREATE TABLE `order` (
  `id` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) NOT NULL,
  `pizza_type` varchar(255) NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `voucher_code` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order`
--

INSERT INTO `order` (`id`, `created_at`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `updated_at`, `voucher_code`, `user_id`) VALUES
(1, '2022-11-03 14:55:08', 'sd54ca94', 180000, 'M', 'Hawaii', 200000, '2022-11-04 22:12:14', '12334', 1),
(2, '2022-11-03 20:56:01', 'a6uycv5h', 250000, 'Bacon', 'L', 250000, '2022-11-03 20:56:01', NULL, 2),
(3, '2022-11-03 14:56:37', 'dvnv8asd', 150000, 'S', 'Seafood', 150000, '2022-11-03 20:56:37', NULL, 1),
(98, '2022-11-04 22:17:18', '4as8g8ay', 200000, 'M', 'Seafood', 200000, NULL, '', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `region`
--

CREATE TABLE `region` (
  `id` bigint(20) NOT NULL,
  `region_code` varchar(255) DEFAULT NULL,
  `region_name` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `region`
--

INSERT INTO `region` (`id`, `region_code`, `region_name`, `country_id`) VALUES
(1, 'HCM', 'Hồ Chí Minh', 1),
(2, 'HN', 'Hà Nội', 1),
(3, 'DN', 'Đà Nẵng', 1),
(4, 'CT', 'Cần Thơ', 1),
(5, 'CA', 'California', 3),
(6, 'TX', 'Texas', 3),
(7, 'NY', 'New York', 3),
(8, 'FL', 'Florida', 3),
(9, 'SA', 'South Australia', 2),
(10, 'QL', 'QueenLand', 2),
(11, 'TAS', 'Tasmania', 2),
(12, 'VIC', 'Victoria', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `address`, `created_at`, `email`, `fullname`, `phone`, `updated_at`) VALUES
(1, 'HCM', '2022-11-03 20:43:27', 'thent@devcamp.edu.vn', 'Trịnh Thế', '0123456789', '2022-11-03 20:43:27'),
(2, 'Hà Nội', '2022-11-03 20:44:05', 'phuongkieu@devcamp.edu.vn', 'Kiều Phương', '0125874963', '2022-11-03 20:44:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `voucher`
--

CREATE TABLE `voucher` (
  `id` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `ma_voucher` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phan_tram_giam_gia` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci ROW_FORMAT=DYNAMIC;

--
-- Đang đổ dữ liệu cho bảng `voucher`
--

INSERT INTO `voucher` (`id`, `ghi_chu`, `ma_voucher`, `phan_tram_giam_gia`, `updated_at`, `created_at`) VALUES
(1, 'Giảm 10%', '16512', '10', '2022-11-04 15:28:22', NULL),
(2, 'Giảm 10%', '12354', '10', '2022-11-04 15:28:49', NULL),
(3, NULL, '12332', '10', NULL, NULL),
(4, NULL, '12332', '10', NULL, NULL),
(5, NULL, '95531', '10', NULL, NULL),
(6, NULL, '81432', '10', NULL, NULL),
(7, NULL, '15746', '10', NULL, NULL),
(8, NULL, '76241', '10', NULL, NULL),
(9, NULL, '64562', '10', NULL, NULL),
(10, NULL, '25896', '10', NULL, NULL),
(11, NULL, '87654', '20', NULL, NULL),
(12, NULL, '86423', '20', NULL, NULL),
(13, NULL, '46253', '20', NULL, NULL),
(14, NULL, '46211', '20', NULL, NULL),
(15, NULL, '36594', '20', NULL, NULL),
(16, NULL, '24864', '20', NULL, NULL),
(17, NULL, '23156', '20', NULL, NULL),
(18, NULL, '13946', '20', NULL, NULL),
(19, NULL, '34962', '20', NULL, NULL),
(20, NULL, '26491', '20', NULL, NULL),
(21, NULL, '94634', '30', NULL, NULL),
(22, NULL, '87643', '30', NULL, NULL),
(23, NULL, '61353', '30', NULL, NULL),
(24, NULL, '64532', '30', NULL, NULL),
(25, NULL, '89436', '30', NULL, NULL),
(26, NULL, '73256', '30', NULL, NULL),
(27, NULL, '21561', '30', NULL, NULL),
(28, NULL, '35468', '30', NULL, NULL),
(29, NULL, '32486', '30', NULL, NULL),
(30, NULL, '96462', '40', NULL, NULL),
(31, NULL, '41356', '40', NULL, NULL),
(32, NULL, '76164', '40', NULL, NULL),
(33, NULL, '72156', '40', NULL, NULL),
(34, NULL, '46594', '40', NULL, NULL),
(35, NULL, '35469', '40', NULL, NULL),
(36, NULL, '34546', '40', NULL, NULL),
(37, NULL, '34862', '40', NULL, NULL),
(38, NULL, '14652', '40', NULL, NULL),
(39, NULL, '40685', '40', NULL, NULL),
(40, NULL, '92061', '50', NULL, NULL),
(41, '', '70056', '50', '2022-11-04 15:24:50', NULL),
(42, NULL, '41603', '50', NULL, NULL),
(43, NULL, '80320', '50', NULL, NULL),
(44, NULL, '40381', '50', NULL, NULL),
(45, NULL, '44306', '50', NULL, NULL),
(46, NULL, '10641', '50', NULL, NULL),
(47, NULL, '70651', '50', NULL, NULL),
(48, NULL, '80325', '50', NULL, NULL),
(49, NULL, '50516', '50', NULL, NULL),
(50, NULL, '10056', '50', NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oqixmig4k8qxc8oba3fl4gqkr` (`country_code`);

--
-- Chỉ mục cho bảng `drink`
--
ALTER TABLE `drink`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_n5jclk5gudnbqmdsfytx8lyny` (`order_code`),
  ADD KEY `FK5ds52cnxjw9c99ovccne0axk0` (`user_id`);

--
-- Chỉ mục cho bảng `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_is5udyhip6itt1wt8tj8hx5wq` (`region_code`),
  ADD KEY `FK7vb2cqcnkr9391hfn72louxkq` (`country_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  ADD UNIQUE KEY `UK_589idila9li6a4arw1t8ht1gx` (`phone`);

--
-- Chỉ mục cho bảng `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `country`
--
ALTER TABLE `country`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `region`
--
ALTER TABLE `region`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `FK5ds52cnxjw9c99ovccne0axk0` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FK7vb2cqcnkr9391hfn72louxkq` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
