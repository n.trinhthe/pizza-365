package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.User;

public interface IUserRepository extends JpaRepository<User,Long> {
    Optional<User> findById(Long id);
}
