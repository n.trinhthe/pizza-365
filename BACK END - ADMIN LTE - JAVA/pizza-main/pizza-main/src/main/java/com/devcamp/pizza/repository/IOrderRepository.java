package com.devcamp.pizza.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza.models.Order;

public interface IOrderRepository extends JpaRepository<Order,Long>{
    Optional<Order> findById(Long id);
}
