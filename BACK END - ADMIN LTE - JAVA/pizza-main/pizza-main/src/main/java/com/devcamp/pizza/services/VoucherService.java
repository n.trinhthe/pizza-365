package com.devcamp.pizza.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza.models.Voucher;
import com.devcamp.pizza.repository.IVoucherRepository;

@Service
public class VoucherService {
    @Autowired
    IVoucherRepository voucherRepository;

    public List<Voucher> getAllVoucher() {
        ArrayList<Voucher> listVoucher = new ArrayList<>();
        voucherRepository.findAll().forEach(listVoucher::add);
        return listVoucher;
    }

    public Voucher getVoucherById(Long id) {
        Optional<Voucher> voucherData = voucherRepository.findById(id);
        if (voucherData.isPresent()) {
            return voucherData.get();
        } else {
            return null;
        }
    }

    public Voucher createVoucher(Voucher pVoucher) {
        Voucher newVoucher = new Voucher();
        newVoucher.setMaVoucher(pVoucher.getMaVoucher());
        newVoucher.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
        newVoucher.setGhiChu(pVoucher.getGhiChu());
        newVoucher.setNgayTao(new Date());
        voucherRepository.save(newVoucher);
        return newVoucher;
    }

    public Voucher updateVoucher(Voucher pVoucher, Long id) {
        Optional<Voucher> voucherData = voucherRepository.findById(id);
        if (voucherData.isPresent()) {
            Voucher updatedVoucher = voucherData.get();
            updatedVoucher.setMaVoucher(pVoucher.getMaVoucher());
            updatedVoucher.setPhanTramGiamGia(pVoucher.getPhanTramGiamGia());
            updatedVoucher.setGhiChu(pVoucher.getGhiChu());
            updatedVoucher.setNgayCapNhat(new Date());
            voucherRepository.save(updatedVoucher);
            return updatedVoucher;
        } else {
            return null;
        }
    }

    public void deleteVoucher(Long id){
        voucherRepository.deleteById(id);
    }
}
